**1. Sobre o Projeto Garbage Collector**
---
A ideia surge no âmbito de cidades inteligentes. Mais concretamente no problema de recolha de resíduos sólidos nas cidades que têm vários ecopontos subterrâneos.<p>
Em algumas cidades não existe um sistema que indique o estado de ocupação dos contentores, o que leva a que os camionistas passem de forma periódica pelos mesmos contentores, estando estes cheios ou vazios, e sempre pela mesma rota, que pode não ser a mais eficaz para a recolha mais rápida e com maior taxa de ocupação do camião.


**2. Solução**
---
Para resolver este desafio, propomos o desenvolvimento de um sensor de deteção de proximidade de forma a descobrir quais os contentores subterrâneos que contêm uma taxa de ocupação maior e necessitam de ser recolhidos.<p>
Assim como uma Web app com indicação do estado dos contentores para ser possível avisar a população dos contentores ainda disponíveis para recolha em caso de eventos que condicionem o normal funcionamento.<p> 
Um sistema de notificação para os camionistas avisarem o sistema da existência de lixo não recolhido por falta de espaço ou, por exemplo, avisar sobre aquele lixo que não está dentro do ecoponto.<p> 
Também um sistema de notificação para a população avisar no caso de colocar algum tipo de resíduo fora do ecoponto, identificando o tipo de resíduo e quando, permitindo dessa forma incorporar esse lixo na rota de recolha do camionista.<p> 
Por último mas não menos importante, geração de rota inteligente para recolha de resíduos, maximizando a capacidade do camião e os resíduos recolhidos.


**3. Funcionalidades**
---
*  Deteção da taxa de ocupação de um contentor mediante o uso de um sensor de proximidade
*  Site para visualização de um mapa com os diferentes ecopontos (com diferentes cores mediante a sua taxa de ocupação)
    1. Para a população em geral (para consultar o estado dos contentores)
    2. Para os camionistas escolherem os contentores a ser recolhidos ou optarem por gerar a rota de recolha inteligente
    3. Possibilidade de notificar o sistema sobre lixo ainda não recolhido para posterior ser incorporado numa rota
*  Geração de rota de recolha inteligente
*  Registo e autenticação de entidades responsáveis pela recolha de resíduos
*  Conta com privilégios com a possibilidade de gestão de todas as entidades envolvidas, como por exemplo, os camionistas e os ecopontos, através das operações de CRUD


**4. Arquitetura**
---
<div align="center">
![Image of Yaktocat](Epoca_Normal/Documentacao/Arquitetura.png)
</div>