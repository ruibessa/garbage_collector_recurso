﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
using WebApi.Helpers;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using WebApi.Services;
using WebApi.Entities;
using WebApi.Models.Ecoponto;
//using Microsoft.AspNetCore.Authentication.JwtBearer;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class EcopontosController : ControllerBase
    {
        private IEcopontoService _ecopontoService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public EcopontosController(
            IEcopontoService userService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _ecopontoService = userService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("create")]
        public IActionResult Create([FromBody]CriarEcoponto model)
        {
            // map model to entity
            var ecoponto = _mapper.Map<Ecoponto>(model);

            //Calcular o estado (taxa de ocupacao em %) do ecoponto
            ecoponto.Estado = ((ecoponto.Altura - ecoponto.AlturaLivre) * 100) / ecoponto.Altura;
            ecoponto.Estado = Math.Round(ecoponto.Estado, 2);

            try
            {
                _ecopontoService.Create(ecoponto);
                return Ok(ecoponto);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        //[Authorize(Roles = Role.Admin)]
        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {

            var ecopontos = _ecopontoService.GetAll();
            var model = _mapper.Map<IList<EcopontoModel>>(ecopontos);
            return Ok(model);
        }

        [HttpGet("rota/{id}")]
        public IActionResult GetRota(int id)
        {
            var ecopontos = _ecopontoService.GetRota(50);
            var model = _mapper.Map<IList<EcopontoModel>>(ecopontos);

            double somaCapacidadeEcopontos = 0;

            foreach (EcopontoModel eco in model)
            {
                Console.WriteLine("ALTURA - - - - " + eco.Altura);
                Console.WriteLine("LIVRE - - - - " + eco.AlturaLivre);
                Console.WriteLine("CAPACIDADE - - - - " + eco.Capacidade);

                somaCapacidadeEcopontos += (eco.Capacidade * ((eco.Altura - eco.AlturaLivre) * 100) / eco.Altura) / 100;

                Console.WriteLine("SOMA - - - - " + somaCapacidadeEcopontos);
            }
            Console.WriteLine("SOMA - - - - " + somaCapacidadeEcopontos);

            // ecopontos = _ecopontoService.GetRota(75);
            // model = _mapper.Map<IList<EcopontoModel>>(ecopontos);

            return Ok(model);
        }

        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            // only allow admins to access other user records
            // var currentUserId = int.Parse(User.Identity.Name);
            // if (id != currentUserId && !User.IsInRole(Role.Admin))
            //     return Forbid();

            var ecoponto = _ecopontoService.GetById(id);
            var model = _mapper.Map<EcopontoModel>(ecoponto);

            return Ok(model);
        }

        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]AtualizarEcoponto model)
        {
            // map model to entity and set id
            var ecoponto = _mapper.Map<Ecoponto>(model);
            ecoponto.Id = id;

            //Calcular o estado (taxa de ocupacao em %) do ecoponto
            ecoponto.Estado = ((ecoponto.Altura - ecoponto.AlturaLivre) * 100) / ecoponto.Altura;
            ecoponto.Estado = Math.Round(ecoponto.Estado, 2);

            try
            {
                // update ecoponto 
                _ecopontoService.Update(ecoponto);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _ecopontoService.Delete(id);
            return Ok();
        }
    }
}
