using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Helpers;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class LocalidadeController : ControllerBase
    {
        private ILocalidadesService _localidadeService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public LocalidadeController(
            ILocalidadesService localidadeService,
            IMapper mapper,
            IOptions<AppSettings> appSettings
        ){
            _localidadeService = localidadeService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetDistritos()
        {
            var localidades = _localidadeService.GetAllDistritos();
            return Ok(localidades);
        }

        [AllowAnonymous]
        [HttpGet("concelho")]
        public IActionResult GetConcelhos(int distrito)
        {
            var concelhos = _localidadeService.GetAllConcelhos(distrito);
            return Ok(concelhos);
        }

        [AllowAnonymous]
        [HttpGet("freguesia")]
        public IActionResult GetFreguesias(int concelho)
        {
            var freguesias = _localidadeService.GetAllFreguesias(concelho);
            return Ok(freguesias);
        }
    }
}