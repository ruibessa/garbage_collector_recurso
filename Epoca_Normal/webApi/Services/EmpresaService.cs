using System;
using System.IO;
using System.Linq;
using System.Net;
using Microsoft.Extensions.Options;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IEmpresaService
    {
        User CreateCompany(User user, string password, Empresa empresa);
    }

    public class EmpresaService : IEmpresaService
    {

        private DataContext _context;
        private readonly AppSettings _appSettings;

        public EmpresaService(DataContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        public void VerifyNif(string numero)
        {
            // Create a new 'HttpWebRequest' object to the mentioned URL.
            HttpWebRequest myHttpWebRequest = (HttpWebRequest)WebRequest.Create("https://www.nif.pt/?json=1&q=" + numero + "&key=13acc4fbe80c200cf2f46d6bff9a8081");
            myHttpWebRequest.UserAgent = ".NET Framework Test Client";

            // Assign the response object of 'HttpWebRequest' to a 'HttpWebResponse' variable.
            HttpWebResponse myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();

            // Display the contents of the page to the console.
            Stream streamResponse = myHttpWebResponse.GetResponseStream();
            StreamReader streamRead = new StreamReader(streamResponse);

            Char[] readBuff = new Char[999];
            int count = streamRead.Read(readBuff, 0, 999);
            String outputData = new String(readBuff, 0, count);
            String result = outputData.Split(",")[0];
            String finals = result.Split(":")[1].ToString();
            // Release the response object resources.
            streamRead.Close();
            streamResponse.Close();
            myHttpWebResponse.Close();

            char[] c = finals.ToCharArray();
            char[] d = new char[c.Length - 2];
            for (int i = 1; i < c.Length - 1; i++)
            {
                d[i - 1] = c[i];
            }
            finals = new String(d, 0, d.Length - 1);

            if (finals == "erro")
                throw new AppException("Nif not existent");
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        public User CreateCompany(User user, string password, Empresa empresa)
        {

            if(string.IsNullOrWhiteSpace(user.Empresa))
                user.Empresa = empresa.Nif;
            else if(string.IsNullOrWhiteSpace(empresa.Nif))
                empresa.Nif = user.Empresa;
            // validation
            if (string.IsNullOrWhiteSpace(password))
                throw new AppException("Password is required");

            if (_context.Users.Any(x => x.Username == user.Username))
                throw new AppException("Username \"" + user.Username + "\" is already taken");
            
            if (_context.Empresas.Any(x => x.Nif == empresa.Nif))
                throw new AppException("Nif \"" + empresa.Nif + "\" is alredy taken");
            
            //Temporariamente desativado para poder realizar testes
            this.VerifyNif(empresa.Nif);

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            _context.Empresas.Add(empresa);
            _context.Users.Add(user);
            _context.SaveChanges();

            return user;
        }
    }
}