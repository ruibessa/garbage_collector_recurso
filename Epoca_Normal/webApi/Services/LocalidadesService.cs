using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface ILocalidadesService
    {
        IEnumerable<Localidade> GetAllDistritos();
        IEnumerable<Localidade> GetAllConcelhos(int distrito);
        IEnumerable<Localidade> GetAllFreguesias(int concelho);
    }
    public class LocalidadeService : ILocalidadesService
    {
        private DataContext _context;
        private readonly AppSettings _appSettings;

        public LocalidadeService(DataContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        public IEnumerable<Localidade> GetAllDistritos()
        {
            return _context.Localidades.FromSqlRaw($"SELECT * FROM Localidades Where parent_id=0").ToList();
        }

        public IEnumerable<Localidade> GetAllConcelhos(int distrito)
        {
            if (_context.Localidades.Any(x => x.id == distrito && x.parent_id == 0 && x.level == 1))
                return _context.Localidades.FromSqlRaw($"SELECT * FROM Localidades Where parent_id ='{distrito}' AND level = 2").ToList();
            else
            {
                throw new AppException("Distrito não encontrado");
            }
        }

        public IEnumerable<Localidade> GetAllFreguesias(int concelho)
        {
            if (_context.Localidades.Any(x => x.id == concelho && x.parent_id != 0 && x.level == 2))
                return _context.Localidades.FromSqlRaw($"SELECT * FROM Localidades Where parent_id='{concelho}' AND level = 3").ToList();
            else
            {
                throw new AppException("Concelho não encontrado não encontrado");
            }
            
        }
    }
}