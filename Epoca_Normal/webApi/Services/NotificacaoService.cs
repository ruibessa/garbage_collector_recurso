using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Notificacao;

namespace WebApi.Services
{
    public interface INotificacaoService
    {
        IEnumerable<Notificacao> GetAll();
        Notificacao GetById(int id);
        Notificacao Register(Notificacao notificacao);
        void Update(Notificacao notificacao);
        void Delete(int id);
    }

    public class NotificacaoService : INotificacaoService
    {

        private DataContext _context;
        private readonly AppSettings _appSettings;

        public NotificacaoService(DataContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        public IEnumerable<Notificacao> GetAll()
        {
            return _context.Notificacoes.FromSqlRaw("SELECT * FROM Notificacoes");
        }

        public Notificacao GetById(int id)
        {
            return _context.Notificacoes.Find(id);
        }

        public Notificacao Register(Notificacao notificacao)
        {
            if (string.IsNullOrWhiteSpace(notificacao.Descricao))
                throw new AppException("Descrição em falta!");

            if (string.IsNullOrWhiteSpace(notificacao.Latitude))
                throw new AppException("Latitude em falta!");

            if (string.IsNullOrWhiteSpace(notificacao.Latitude))
                throw new AppException("Longitude em falta!");

            if (string.IsNullOrWhiteSpace(notificacao.Imagem))
                throw new AppException("Imagem em falta!");

            //Se já existe o Notificacao
            if (_context.Notificacoes.Any(x => x == notificacao))
                throw new AppException("Notificacao ja existe!!");

            _context.Notificacoes.Add(notificacao);
            _context.SaveChanges();

            return notificacao;
        }

        public void Update(Notificacao updates)
        {
            var old = _context.Notificacoes.Find(updates.Id);

            if (old == null)
                throw new AppException("Notificacao nao encontrada");

            if (!string.IsNullOrWhiteSpace(updates.Descricao) && !old.Descricao.Equals(updates.Descricao))
                old.Descricao = updates.Descricao;

            if (!string.IsNullOrWhiteSpace(updates.Imagem) && !old.Imagem.Equals(updates.Imagem))
                old.Imagem = updates.Imagem;

            if (!string.IsNullOrWhiteSpace(updates.Latitude) && !old.Latitude.Equals(updates.Latitude))
                old.Latitude = updates.Latitude;

            if (!string.IsNullOrWhiteSpace(updates.Longitude) && !old.Longitude.Equals(updates.Longitude))
                old.Longitude = updates.Longitude;

            _context.Notificacoes.Update(old);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var notificacao = _context.Notificacoes.Find(id);
            if (notificacao != null)
            {
                _context.Notificacoes.Remove(notificacao);
                _context.SaveChanges();
            }
            else
            {
                throw new AppException("Não foi possivel encontar esta notificação");
            }
        }
    }
}