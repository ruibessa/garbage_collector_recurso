using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WebApi.Entities;
using WebApi.Helpers;
using static WebApi.Program;

namespace WebApi.Services
{
    public interface IEcopontoService
    {
        IEnumerable<Ecoponto> GetAll();

        IEnumerable<Ecoponto> GetRota(int taxaOcupacao);
        Ecoponto GetById(int id);
        Ecoponto Create(Ecoponto ecoponto);
        void Update(Ecoponto ecoponto);
        void Delete(int id);
    }

    public class EcopontoService : IEcopontoService
    {
        static Semaphore sem = new Semaphore(1, 1);
        private double Distancia;
        private static bool estado = true;
        private static bool estadoServer = true;
        private TcpClient client;
        private NetworkStream stream;
        private DataContext _context;
        private readonly AppSettings _appSettings;

        public EcopontoService(DataContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
            Distancia = 0.0;
        }

        public IEnumerable<Ecoponto> GetAll()
        {
            if (estado)
            {
                getDistance();
                estado = false;
                sem.WaitOne();
                Ecoponto e = _context.Ecopontos.Find(2);
                if (e != null)
                {
                    e.AlturaLivre = Distancia;
                    _context.Ecopontos.Update(e);
                    _context.SaveChanges();
                }
                sem.Release();
            }

            return _context.Ecopontos;
        }

        public IEnumerable<Ecoponto> GetRota(int taxaOcupacao)
        {
            return _context.Ecopontos.FromSqlRaw($"SELECT * FROM Ecopontos Where Estado>'{taxaOcupacao}'").ToList();
        }

        public Ecoponto GetById(int id)
        {
            return _context.Ecopontos.Find(id);
        }

        public Ecoponto Create(Ecoponto ecoponto)
        {
            // validation TODO ou APAGAR
            // if (string.IsNullOrWhiteSpace(ecoponto.Capacidade))
            //     throw new AppException("Capacidade is required");

            //FALTA FAZER MAIS VALIDAÇÕES!!!! #TODO

            //Se já existe o Ecoponto
            if (_context.Ecopontos.Any(x => x == ecoponto))
                throw new AppException("Ecoponto already exists!!");

            _context.Ecopontos.Add(ecoponto);
            _context.SaveChanges();

            return ecoponto;
        }

        //#TODO VERIFICAR SE FUNCIONA O UPDATE
        public void Update(Ecoponto ecopontoParam)
        {
            var ecoponto = _context.Ecopontos.Find(ecopontoParam.Id);

            if (ecoponto == null)
                throw new AppException("Ecoponto not found");

            ecoponto.Capacidade = ecopontoParam.Capacidade;
            ecoponto.CodigoPostal = ecopontoParam.CodigoPostal;
            ecoponto.Concelho = ecopontoParam.Concelho;
            ecoponto.Distrito = ecopontoParam.Distrito;
            ecoponto.Estado = ecopontoParam.Estado;
            ecoponto.Freguesia = ecopontoParam.Freguesia;
            ecoponto.Rua = ecopontoParam.Rua;
            ecoponto.Sensor = ecopontoParam.Sensor;
            ecoponto.Tipo = ecopontoParam.Tipo;
            ecoponto.lat = ecopontoParam.lat;
            ecoponto.lon = ecopontoParam.lon;
            ecoponto.Altura = ecopontoParam.Altura;
            ecoponto.AlturaLivre = ecopontoParam.AlturaLivre;

            _context.Ecopontos.Update(ecoponto);
            _context.SaveChanges();
        }

        public void Delete(int id)
        {
            var ecoponto = _context.Ecopontos.Find(id);
            if (ecoponto != null)
            {
                _context.Ecopontos.Remove(ecoponto);
                _context.SaveChanges();
            }
        }

        public void getDistance()
        {
            // Creating object of ExThread class 
            sem.WaitOne();
            ExThread obj = new ExThread();
            Thread thr = new Thread(new ThreadStart(mythread1));
            thr.Start();
        }

        public void mythread1()
        {
            //IPv4 do PC
            string ip = "192.168.1.45";
            int port = 11000;
            TcpListener server = new TcpListener(IPAddress.Parse(ip), port);
            Console.WriteLine("Servidor ligado no ip:porta " + ip + ":" + port);
            Console.WriteLine("A espera do cliente");
            switch (estadoServer)
            {
                case true:
                    Console.WriteLine("A espera do cliente");
                    estadoServer = false;
                    Console.WriteLine("Raspberry conectado 2");
                    server.Start();
                    client = server.AcceptTcpClient();
                    stream = client.GetStream();
                    break;
            }

            String data = null;
            try
            {
                while (!stream.DataAvailable) ;
                System.Threading.Thread.Sleep(1000);
                Byte[] bytes = new Byte[client.Available];
                int inteiro = stream.Read(bytes, 0, bytes.Length);

                data = System.Text.Encoding.ASCII.GetString(bytes, 0, inteiro);
                Console.WriteLine("Valor: {0}", data);
                Distancia = double.Parse(data, System.Globalization.CultureInfo.InvariantCulture);
                sem.Release();

            }
            catch (IOException e)
            {
                Console.WriteLine($"Erro: '{e}'");
            }
            finally
            {
                Distancia = double.Parse(data, System.Globalization.CultureInfo.InvariantCulture); ;
                server.Stop();
            }

        }
    }
}


