﻿using System;
using System.Threading;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace WebApi
{
    public class Program
    {

        public class ExThread
        {

            // Non-static method 
            public void mythread1()
            {
                for (int z = 0; z < 3; z++)
                {
                    Console.WriteLine("This is of the second Thread!");
                    Thread.Sleep(4000);
                }
            }
        }

        public static void Main(string[] args)
        {
            // Creating object of ExThread class 
            //ExThread obj = new ExThread();

            // Creating thread 
            // Using thread class 
            //Thread thr = new Thread(new ThreadStart(obj.mythread1));
            //thr.Start();
            Console.WriteLine("ANTES DA \"THREAD\" PRINCIPAL - - - INICIO .-- - - - - - --");
            CreateHostBuilder(args).Build().Run();
            Console.WriteLine("APAGAR ISTO APARECE NO FINAL DA EXECUÇÃO DO PROGRAMA!!!! POR EXEMPLO AQUANDO: CRTL + C!!");
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>()
                        .UseUrls("http://localhost:4000");
                });
    }
}
