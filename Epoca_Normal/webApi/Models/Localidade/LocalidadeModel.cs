using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Localidade
{
    public class LocalidadeModel
    {
        [Required]
        public int id { get; set; }
        [Required]
        public int parent_id { get; set; }
        [Required]
        public int level { get; set; }
        [Required]
        public string name { get; set; }
    }
}