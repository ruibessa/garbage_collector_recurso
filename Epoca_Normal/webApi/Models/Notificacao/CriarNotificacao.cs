using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Notificacao
{
    public class CriarNotificacao
    {
        [Required]
        public string Latitude { get; set; }
        [Required]
        public string Longitude { get; set; }
        [Required]
        public string Descricao { get; set; }
        [Required]
        public string Imagem { get; set; }
        [Required]
        public int UserId { get; set; }
    }
}