namespace WebApi.Entities
{
    public static class Role
    {
        public const string Admin = "Admin";
        public const string Gestor_Chefe = "Gestor_Chefe";
        public const string Gestor = "Gestor";
        public const string Camionista = "Camionista";
        public const string Cidadao = "Cidadao";
    }
}