// namespace WebApi.Helpers
// {
//     /**
//      *
//      * @param <T>
//      */
//     public class Network<T>
//     {

//         /**
//          * matriz de adjacencias
//          */
//         protected double[][] adjMatrixDouble;
//         private int numeroTotalSkills = 0;

//         public double[][] getAdjMatrixDouble()
//         {
//             return adjMatrixDouble;
//         }

//         public void setAdjMatrixDouble(double[][] matrizRecebida)
//         {
//             this.adjMatrixDouble = matrizRecebida;
//         }

//         /**
//          * método construtor vazio
//          */
//         public Network()
//         {
//             numVertices = 0;
//             this.adjMatrixDouble = new double[DEFAULT_CAPACITY][DEFAULT_CAPACITY];
//             this.vertices = (T[])(new Object[DEFAULT_CAPACITY]);
//         }

//         /**
//          * método construtor com tamanho recebido
//          *
//          * @param size
//          */
//         public NetworkPlataformaSocialImpl(int size)
//         {
//             numVertices = 0;
//             this.adjMatrixDouble = new double[size][size];
//             this.vertices = (T[])(new Object[size]);
//             for (int i = 0; i < size; i++)
//             {
//                 for (int j = size - 1; j >= 0; j--)
//                 {
//                     adjMatrixDouble[i][j] = Double.POSITIVE_INFINITY;
//                 }
//             }
//         }

//         public void totalSkillsNetwork(int skill)
//         {
//             this.numeroTotalSkills += skill;
//         }

//         public double mediaSkills()
//         {
//             System.out.println(this.numeroTotalSkills);
//             return (this.numeroTotalSkills / this.numVertices);
//         }

//         private void atualizarMencoesPessoa()
//         {

//             for (int pessoaAtual = 0; pessoaAtual < this.numVertices; pessoaAtual++)
//             { //para cada pessoa

//                 for (int contato : ((Pessoa)this.vertices[pessoaAtual]).getContacts())
//                 { //para cada contato da pessoa

//                     for (int mencao : ((Pessoa)this.vertices[contato]).getMencoes())
//                     { //para cada mencao do contato

//                         if (mencao == pessoaAtual)
//                         { //verificar se menciona a pessoa

//                             ((Pessoa)this.vertices[pessoaAtual]).addMencao(); //adiciona uma mencao
//                         }
//                     }
//                 }
//             }
//         }

//         public double[][] atualizaMatrizComMencoes()
//         {

//             double[][] temporariaMatriz = new double[this.numVertices][this.numVertices];

//             /**
//              * criar matriz temporaria para retorno
//              */
//             for (int i = 0; i < numVertices; i++)
//             {
//                 for (int j = 0; j < numVertices; j++)
//                 {
//                     double d = this.adjMatrixDouble[i][j];
//                     temporariaMatriz[i][j] = d;
//                 }
//             }

//             atualizarMencoesPessoa(); //atualizar numero de menções de cada pessoa

//             /**
//              * atualizar peso da matriz original
//              */
//             for (int linha = 0; linha < this.numVertices; linha++)
//             {

//                 for (int coluna = 0; coluna < this.numVertices; coluna++)
//                 {

//                     if (adjMatrixDouble[linha][coluna] < Double.POSITIVE_INFINITY)
//                     { //se tiver contato

//                         if (((Pessoa)this.vertices[coluna]).getNumeroMencoes() != 0)
//                         { //se tiver menções

//                             adjMatrixDouble[linha][coluna] = (1.0 / ((Pessoa)this.vertices[coluna]).getNumeroMencoes());

//                         }
//                         else
//                         { //se não tiver menções
//                             adjMatrixDouble[linha][coluna] = 1;
//                         }
//                     }
//                 }
//             }

//             System.out.println(this.toString());

//             return temporariaMatriz; //devolver matriz temporaria
//         }

//         /**
//          * adiciona pessoa à network e atualiza todos os pesos com os contactos na
//          * matriz de adjacencias
//          *
//          * @param pessoa
//          */
//         public void addVertexComPeso(T pessoa)
//         {

//             /**
//              * se a network estiver cheia é necessário expandir a capacidade
//              */
//             if (numVertices == vertices.length)
//             {
//                 expandCapacity();
//             }

//             vertices[numVertices] = pessoa; //adiciona a pessoa
//             numVertices++; //incremente numero de pessoas

//             /**
//              * para cada contacto adiciona o peso de custo do conhecido para esta
//              * pessoa
//              */
//             for (int contato : ((Pessoa)pessoa).getContacts())
//             {
//                 adjMatrixDouble[(int)contato][getIndex(pessoa)] = (1.0 / ((Pessoa)pessoa).getVisualizacoes());
//             }
//         }

//         /**
//          * altera todos os pesos com o peso dado pelo utilizador
//          *
//          * @param peso
//          * @return
//          */
//         public double[][] alteraTodosPesos(double peso)
//         {

//             double[][] temporariaMatriz = new double[this.numVertices][this.numVertices];

//             /**
//              * criar matriz temporaria para retorno
//              */
//             for (int i = 0; i < numVertices; i++)
//             {
//                 for (int j = 0; j < numVertices; j++)
//                 {
//                     double d = this.adjMatrixDouble[i][j];
//                     temporariaMatriz[i][j] = d;
//                 }
//             }

//             /**
//              * atualizar peso da matriz original
//              */
//             for (int i = 0; i < numVertices; i++)
//             {
//                 for (int j = 0; j < numVertices; j++)
//                 {
//                     if (this.adjMatrixDouble[i][j] < Double.POSITIVE_INFINITY)
//                     {
//                         this.adjMatrixDouble[i][j] = peso;
//                     }
//                 }
//             }

//             return temporariaMatriz; //devolver matriz temporaria
//         }

//         @Override
//         public String consultaUtilizadorPorEmail(int indiceUtilizador)
//         {

//             //adicionar uma visualizacao sempre que se consulta este utilizador
//             ((Pessoa)(this.vertices[indiceUtilizador])).addVisualizacao();

//             //retornar a pessoa
//             return ((Pessoa)vertices[indiceUtilizador]).toString();
//         }

//         /**
//          * Verifica se o grafo é completo ou não
//          *
//          * @return
//          */
//         @Override
//         public String isCompleto()
//         {

//             for (int i = 0; i < numVertices; i++)
//             {

//                 for (int j = 0; j < numVertices; j++)
//                 {

//                     if (adjMatrixDouble[i][j] == Double.POSITIVE_INFINITY && i != j)
//                     {
//                         return "Grafo não completo.";
//                     }
//                 }
//             }
//             return "Grafo completo.";
//         }

//         /**
//          * Verifica se existe caminho entre os dois utilizadores introduzidos e em
//          * caso afirmativo retorna um iterator com os vértices do caminho mais curto
//          *
//          * @param indiceUtilizador1
//          * @param indiceUtilizador2
//          * @return
//          */
//         @Override
//         public Iterator<T> caminhoMaisCurto(int indiceUtilizador1, int indiceUtilizador2)
//         {

//             Iterator<T> it;

//             int index;
//             double peso;
//             int[] predecessores = new int[numVertices]; //predecessor do vertice atual
//             LinkedHeap<Double> minHeap = new LinkedHeap<>(); //para saber o menor peso em cada interação
//             LinkedStack<Integer> stack = new LinkedStack<>(); //para devolver o caminho na ordem certa
//             ArrayUnorderedList<T> resultado = new ArrayUnorderedList<>(); //para retornar o caminho

//             /**
//              * inicializa todas as distancias a infinito
//              */
//             double[] distancias = new double[numVertices];
//             for (int i = 0; i < numVertices; i++)
//             {
//                 distancias[i] = Double.POSITIVE_INFINITY;
//             }

//             /**
//              * inicializa todas os vertices false
//              */
//             boolean[] verticeJaVisitado = new boolean[numVertices];
//             for (int i = 0; i < numVertices; i++)
//             {
//                 verticeJaVisitado[i] = false;
//             }

//             /**
//              * para saber os utilizadores inalcançáveis pelo utilizador de origem
//              */
//             ArrayUnorderedList<T> arrayDesconhecidos = new ArrayUnorderedList<>();
//             T pessoaDesconhecida;

//             it = listarDesconhecidos(indiceUtilizador1);

//             /**
//              * põe todos os vértices que possuem pessoas inalcançaveis a true, para
//              * não os visitar ao calcular o caminho mais curto
//              */
//             while (it.hasNext())
//             {
//                 verticeJaVisitado[verificaPessoa(pessoaDesconhecida = it.next())] = true;
//                 arrayDesconhecidos.addToRear(pessoaDesconhecida);
//             }

//             /**
//              * se o utilizador de destino for inalcançável para o utilizador de
//              * origem então retorna o iterator vazio
//              */
//             if (arrayDesconhecidos.contains(this.vertices[indiceUtilizador2]))
//             {
//                 return resultado.iterator();
//             }

//             /**
//              * verifica se os utilizador são válidos e se a network não está vazia,
//              * em caso negativo retorna o iterator vazio
//              */
//             if (!indexIsValid(indiceUtilizador1) || !indexIsValid(indiceUtilizador2) || this.isEmpty())
//             {
//                 return (Iterator<T>)resultado.iterator();
//             }

//             distancias[indiceUtilizador1] = 0; //inicializa a distancia na posição do vertice da origem a 0
//             predecessores[indiceUtilizador1] = -1; //ainda não existe predecessor
//             verticeJaVisitado[indiceUtilizador1] = true; //vertice já visitado
//             peso = 0; //peso atual a 0

//             /**
//              * Atualiza todas as distâncias para cada vertice em cada iteração.
//              * Inicialmente, todas as distâncias adjacentes ao vértice inicial
//              * encontram-se a infinito
//              */
//             for (int i = 0; i < numVertices; i++)
//             {
//                 if (!verticeJaVisitado[i])
//                 { //se ainda não foi visitado
//                     distancias[i] = distancias[indiceUtilizador1]
//                             + adjMatrixDouble[indiceUtilizador1][i];

//                     //o predecessor atual é o vértice de origem
//                     predecessores[i] = indiceUtilizador1;

//                     /**
//                      * //adiciona todas as distancias à minHeap do vertice de
//                      * origem a todos os outros vértices
//                      */
//                     minHeap.addElement(distancias[i]);
//                 }
//             }

//             do
//             {
//                 peso = (minHeap.removeMin()); //encontra o peso mínimo
//                 minHeap.removeAllElements(); //remove todos os elementos
//                 if (peso == Double.POSITIVE_INFINITY) // se não existe caminho
//                 {
//                     return resultado.iterator(); //retorna o iterator vazio

//                 }
//                 else
//                 {
//                     index = getIndexVerticeNaoVisitadoAdjacenteComPeso(verticeJaVisitado, distancias,
//                             peso, numVertices); //verifica qual o vertice com menor distância
//                     verticeJaVisitado[index] = true; //visitou o vértice encontrado
//                 }

//                 /**
//                  * Atualiza as distâncias para cada vertice que ainda não foi
//                  * visitado assim como os seus adjacentes que têm um menor curso se
//                  * o seu predecessor for o vértice atual. Para cada vértice não
//                  * visitado insere a distância na heap
//                  */
//                 for (int i = 0; i < numVertices; i++)
//                 {
//                     if (!verticeJaVisitado[i])
//                     { //se ainda não foi visitado

//                         /**
//                          * e se a distância neste momento for inferior
//                          */
//                         if ((adjMatrixDouble[index][i] < Double.POSITIVE_INFINITY)
//                                 && (distancias[index] + adjMatrixDouble[index][i]) < distancias[i])
//                         {

//                             //atualiza a distancia
//                             distancias[i] = distancias[index] + adjMatrixDouble[index][i];

//                             //predecessor é o vertice atual, neste caso, o último com a distância mínima encontrada
//                             predecessores[i] = index;
//                         }

//                         //adiciona à heap todas as distâncias
//                         minHeap.addElement(distancias[i]);
//                     }
//                 }

//                 /**
//                  * enquanto a heap não estiver vazia ou até já ter sido encontrado o
//                  * caminho mais curto para o vértice de destino
//                  */
//             } while (!minHeap.isEmpty() && !verticeJaVisitado[indiceUtilizador2]);

//             index = indiceUtilizador2; //indice do utilizador de destino
//             stack.push(index); //introduz o vertice de destino
//             do
//             {
//                 index = predecessores[index]; //predecessor do vertice atual
//                 stack.push(index); //introduz o precedessor na stack

//                 //enquanto não chegar ao vertice de origem
//             } while (index != indiceUtilizador1);

//             while (!stack.isEmpty())
//             {
//                 //adiciona todos os elementos da stack de forma ordenada no array
//                 resultado.addToRear(this.vertices[(stack.pop())]);
//             }

//             return resultado.iterator(); //devolve o iterator com o caminho encontrado
//         }

//         /**
//          * Verifica se existe caminho entre os dois utilizadores introduzidos e em
//          * caso afirmativo retorna um iterator com os vértices do caminho mais curto
//          *
//          * @param indiceUtilizador1
//          * @param indiceUtilizador2
//          * @return
//          */
//         public Iterator<T> caminhoMaisLongo(int indiceUtilizador1, int indiceUtilizador2)
//         {

//             Iterator<T> it;

//             int index;
//             double peso;
//             int[] predecessores = new int[numVertices]; //predecessor do vertice atual
//             LinkedHeap<Double> minHeap = new LinkedHeap<>(); //para saber o menor peso em cada interação
//             LinkedStack<Integer> stack = new LinkedStack<>(); //para devolver o caminho na ordem certa
//             ArrayUnorderedList<T> resultado = new ArrayUnorderedList<>(); //para retornar o caminho

//             ArrayOrderedList<Double> arrayOrdenado = new ArrayOrderedList<>();

//             /**
//              * inicializa todas as distancias a infinito
//              */
//             double[] distancias = new double[numVertices];
//             for (int i = 0; i < numVertices; i++)
//             {
//                 distancias[i] = 0;
//             }

//             /**
//              * inicializa todas os vertices false
//              */
//             boolean[] verticeJaVisitado = new boolean[numVertices];
//             for (int i = 0; i < numVertices; i++)
//             {
//                 verticeJaVisitado[i] = false;
//             }

//             /**
//              * para saber os utilizadores inalcançáveis pelo utilizador de origem
//              */
//             ArrayUnorderedList<T> arrayDesconhecidos = new ArrayUnorderedList<>();
//             T pessoaDesconhecida;

//             it = listarDesconhecidos(indiceUtilizador1);

//             /**
//              * põe todos os vértices que possuem pessoas inalcançaveis a true, para
//              * não os visitar ao calcular o caminho mais curto
//              */
//             while (it.hasNext())
//             {
//                 verticeJaVisitado[verificaPessoa(pessoaDesconhecida = it.next())] = true;
//                 arrayDesconhecidos.addToRear(pessoaDesconhecida);
//             }

//             /**
//              * se o utilizador de destino for inalcançável para o utilizador de
//              * destino então retorna o iterator vazio
//              */
//             if (arrayDesconhecidos.contains(this.vertices[indiceUtilizador2]))
//             {
//                 return resultado.iterator();
//             }

//             /**
//              * verifica se os utilizador são válidos e se a network não está vazia,
//              * em caso negativo retorna o iterator vazio
//              */
//             if (!indexIsValid(indiceUtilizador1) || !indexIsValid(indiceUtilizador2) || this.isEmpty())
//             {
//                 return (Iterator<T>)resultado.iterator();
//             }

//             distancias[indiceUtilizador1] = 0; //inicializa a distancia na posição do vertice da origem a 0
//             predecessores[indiceUtilizador1] = -1; //ainda não existe predecessor
//             verticeJaVisitado[indiceUtilizador1] = true; //vertice já visitado
//             peso = 0; //peso atual a 0

//             /**
//              * Atualiza todas as distâncias para cada vertice em cada iteração.
//              * Inicialmente, todas as distâncias adjacentes ao vértice inicial
//              * encontram-se a infinito
//              */
//             int cont = 0;
//             for (int i = 0; i < numVertices; i++)
//             {
//                 if (!verticeJaVisitado[i])
//                 { //se ainda não foi visitado
//                     if (adjMatrixDouble[indiceUtilizador1][i] < Double.POSITIVE_INFINITY)
//                     {
//                         distancias[i] = distancias[indiceUtilizador1]
//                                 + adjMatrixDouble[indiceUtilizador1][i];

//                         //o predecessor atual é o vértice de origem
//                         predecessores[i] = indiceUtilizador1;

//                         /**
//                          * //adiciona todas as distancias à minHeap do vertice de
//                          * origem a todos os outros vértices
//                          */
//                         //   minHeap.addElement(distancias[i]);
//                         arrayOrdenado.add(distancias[i]);
//                         cont++;
//                     }
//                 }
//             }

//             /**
//              * feito de forma "recursiva"
//              */
//             do
//             {
//                 // peso = (minHeap.removeMin()); //encontra o peso mínimo
//                 peso = arrayOrdenado.removeLast();
//                 // minHeap.removeAllElements(); //remove todos os elementos
//                 arrayOrdenado = new ArrayOrderedList<>();

//                 //if (peso == Double.POSITIVE_INFINITY) // se não existe caminho
//                 if (cont == 0) // se não existe caminho
//                 {
//                     return resultado.iterator(); //retorna o iterator vazio

//                 }
//                 else
//                 {
//                     index = getIndexVerticeNaoVisitadoAdjacenteComPeso(verticeJaVisitado, distancias,
//                             peso, numVertices); //verifica qual o vertice com menor distância
//                     verticeJaVisitado[index] = true; //visitou o vértice encontrado
//                 }

//                 /**
//                  * Atualiza as distâncias para cada vertice que ainda não foi
//                  * visitado assim como os seus adjacentes que têm um menor curso se
//                  * o seu predecessor for o vértice atual. Para cada vértice não
//                  * visitado insere a distância na heap
//                  */
//                 for (int i = 0; i < numVertices; i++)
//                 {
//                     if (!verticeJaVisitado[i])
//                     { //se ainda não foi visitado

//                         /**
//                          * e se a distância neste momento for inferior
//                          */
//                         if ((adjMatrixDouble[index][i] < Double.POSITIVE_INFINITY) //se ha ligação
//                                 && (distancias[index] + adjMatrixDouble[index][i]) > distancias[i])
//                         {

//                             //atualiza a distancia
//                             distancias[i] = distancias[index] + adjMatrixDouble[index][i];

//                             //predecessor é o vertice atual, neste caso, o último com a distância mínima encontrada
//                             predecessores[i] = index;
//                         }

//                         //adiciona à heap todas as distâncias
//                         //   minHeap.addElement(distancias[i]);
//                         arrayOrdenado.add(distancias[i]);
//                     }
//                 }

//                 /**
//                  * enquanto a heap não estiver vazia ou até já ter sido encontrado o
//                  * caminho mais curto para o vértice de destino
//                  */
//                 //} while (!minHeap.isEmpty() && !verticeJaVisitado[indiceUtilizador2]);
//             } while (!arrayOrdenado.isEmpty() && !verticeJaVisitado[indiceUtilizador2]);

//             index = indiceUtilizador2; //indice do utilizador de destino
//             stack.push(index); //introduz o vertice de destino
//             do
//             {
//                 index = predecessores[index]; //predecessor do vertice atual
//                 stack.push(index); //introduz o precedessor na stack

//                 //enquanto não chegar ao vertice de origem
//             } while (index != indiceUtilizador1);

//             while (!stack.isEmpty())
//             {
//                 //adiciona todos os elementos da stack de forma ordenada no array
//                 resultado.addToRear(this.vertices[(stack.pop())]);
//             }

//             return resultado.iterator(); //devolve o iterator com o caminho encontrado
//         }

//         /**
//          * Verifica se existe caminho entre os dois utilizadores introduzidos e em
//          * caso afirmativo retorna um iterator com os vértices do caminho mais curto
//          *
//          * @param indiceUtilizador1
//          * @param indiceUtilizador2
//          * @return
//          */
//         public Iterator<T> caminhoMaisCurtoMencoes(int indiceUtilizador1, int indiceUtilizador2)
//         {

//             Iterator<T> it;

//             int index;
//             double peso;
//             int[] predecessores = new int[numVertices]; //predecessor do vertice atual
//             LinkedHeap<Double> minHeap = new LinkedHeap<>(); //para saber o menor peso em cada interação
//             LinkedStack<Integer> stack = new LinkedStack<>(); //para devolver o caminho na ordem certa
//             ArrayUnorderedList<T> resultado = new ArrayUnorderedList<>(); //para retornar o caminho

//             /**
//              * inicializa todas as distancias a infinito
//              */
//             double[] distancias = new double[numVertices];
//             for (int i = 0; i < numVertices; i++)
//             {
//                 distancias[i] = Double.POSITIVE_INFINITY;
//             }

//             /**
//              * inicializa todas os vertices false
//              */
//             boolean[] verticeJaVisitado = new boolean[numVertices];
//             for (int i = 0; i < numVertices; i++)
//             {
//                 verticeJaVisitado[i] = false;
//             }

//             /**
//              * para saber os utilizadores inalcançáveis pelo utilizador de origem
//              */
//             ArrayUnorderedList<T> arrayDesconhecidos = new ArrayUnorderedList<>();
//             T pessoaDesconhecida;

//             it = listarDesconhecidos(indiceUtilizador1);

//             /**
//              * põe todos os vértices que possuem pessoas inalcançaveis a true, para
//              * não os visitar ao calcular o caminho mais curto
//              */
//             while (it.hasNext())
//             {
//                 verticeJaVisitado[verificaPessoa(pessoaDesconhecida = it.next())] = true;
//                 arrayDesconhecidos.addToRear(pessoaDesconhecida);
//             }

//             /**
//              * se o utilizador de destino for inalcançável para o utilizador de
//              * destino então retorna o iterator vazio
//              */
//             if (arrayDesconhecidos.contains(this.vertices[indiceUtilizador2]))
//             {
//                 return resultado.iterator();
//             }

//             /**
//              * verifica se os utilizador são válidos e se a network não está vazia,
//              * em caso negativo retorna o iterator vazio
//              */
//             if (!indexIsValid(indiceUtilizador1) || !indexIsValid(indiceUtilizador2) || this.isEmpty())
//             {
//                 return (Iterator<T>)resultado.iterator();
//             }

//             distancias[indiceUtilizador1] = 0; //inicializa a distancia na posição do vertice da origem a 0
//             predecessores[indiceUtilizador1] = -1; //ainda não existe predecessor
//             verticeJaVisitado[indiceUtilizador1] = true; //vertice já visitado
//             peso = 0; //peso atual a 0

//             /**
//              * Atualiza todas as distâncias para cada vertice em cada iteração.
//              * Inicialmente, todas as distâncias adjacentes ao vértice inicial
//              * encontram-se a infinito
//              */
//             for (int i = 0; i < numVertices; i++)
//             {
//                 if (!verticeJaVisitado[i])
//                 { //se ainda não foi visitado

//                     if (distancias[i] == Double.MAX_VALUE - 1 || adjMatrixDouble[indiceUtilizador1][i] == Double.MAX_VALUE - 1 || (distancias[indiceUtilizador1]
//                             + adjMatrixDouble[indiceUtilizador1][i] > Double.MAX_VALUE - 2 && adjMatrixDouble[indiceUtilizador1][i] < Double.POSITIVE_INFINITY))
//                     {
//                         distancias[i] = Double.MAX_VALUE - 1;

//                     }
//                     else
//                     {
//                         distancias[i] = distancias[indiceUtilizador1]
//                                 + adjMatrixDouble[indiceUtilizador1][i];
//                     }

//                     //o predecessor atual é o vértice de origem
//                     predecessores[i] = indiceUtilizador1;

//                     /**
//                      * //adiciona todas as distancias à minHeap do vertice de
//                      * origem a todos os outros vértices
//                      */
//                     minHeap.addElement(distancias[i]);
//                 }
//             }

//             do
//             {
//                 peso = (minHeap.removeMin()); //encontra o peso mínimo
//                 minHeap.removeAllElements(); //remove todos os elementos
//                 if (peso == Double.POSITIVE_INFINITY) // se não existe caminho
//                 {
//                     return resultado.iterator(); //retorna o iterator vazio

//                 }
//                 else
//                 {
//                     index = getIndexVerticeNaoVisitadoAdjacenteComPeso(verticeJaVisitado, distancias,
//                             peso, numVertices); //verifica qual o vertice com menor distância
//                     verticeJaVisitado[index] = true; //visitou o vértice encontrado
//                 }

//                 /**
//                  * Atualiza as distâncias para cada vertice que ainda não foi
//                  * visitado assim como os seus adjacentes que têm um menor curso se
//                  * o seu predecessor for o vértice atual. Para cada vértice não
//                  * visitado insere a distância na heap
//                  */
//                 for (int i = 0; i < numVertices; i++)
//                 {
//                     if (!verticeJaVisitado[i])
//                     { //se ainda não foi visitado

//                         /**
//                          * e se a distância neste momento for inferior
//                          */
//                         if ((adjMatrixDouble[index][i] < Double.POSITIVE_INFINITY)
//                                 && (distancias[index] + adjMatrixDouble[index][i]) < distancias[i])
//                         {

//                             if (distancias[i] == Double.MAX_VALUE - 1 || distancias[index] == Double.MAX_VALUE - 1 || adjMatrixDouble[index][i] == Double.MAX_VALUE - 1 || (distancias[index]
//                                     + adjMatrixDouble[index][i] > Double.MAX_VALUE - 2 && (distancias[index] + adjMatrixDouble[index][i]) < distancias[i] && adjMatrixDouble[index][i] < Double.POSITIVE_INFINITY))
//                             {

//                                 distancias[i] = Double.MAX_VALUE - 1;

//                             }
//                             else
//                             {
//                                 //atualiza a distancia
//                                 distancias[i] = distancias[index] + adjMatrixDouble[index][i];
//                             }

//                             //predecessor é o vertice atual, neste caso, o último com a distância mínima encontrada
//                             predecessores[i] = index;
//                         }

//                         //adiciona à heap todas as distâncias
//                         minHeap.addElement(distancias[i]);
//                     }
//                 }

//                 /**
//                  * enquanto a heap não estiver vazia ou até já ter sido encontrado o
//                  * caminho mais curto para o vértice de destino
//                  */
//             } while (!minHeap.isEmpty() && !verticeJaVisitado[indiceUtilizador2]);

//             index = indiceUtilizador2; //indice do utilizador de destino
//             stack.push(index); //introduz o vertice de destino
//             do
//             {
//                 index = predecessores[index]; //predecessor do vertice atual
//                 stack.push(index); //introduz o precedessor na stack

//                 //enquanto não chegar ao vertice de origem
//             } while (index != indiceUtilizador1);

//             while (!stack.isEmpty())
//             {
//                 //adiciona todos os elementos da stack de forma ordenada no array
//                 resultado.addToRear(this.vertices[(stack.pop())]);
//             }

//             return resultado.iterator(); //devolve o iterator com o caminho encontrado
//         }

//         /**
//          *
//          * Returns the index of the the vertex that that is adjacent to the vertex
//          * with the given index and also has a pathWeight equal to weight. Retorna
//          * qual o index (posicao no array de vertices) é que se encontra o vértice
//          * com o peso enviado e ainda não foi visitado
//          *
//          * @param verticeJaVisitado
//          * @param distancias
//          * @param peso
//          * @param numVertices
//          * @return
//          */
//         private int getIndexVerticeNaoVisitadoAdjacenteComPeso(boolean[] verticeJaVisitado,
//                 double[] distancias, double peso, int numVertices)
//         {

//             for (int i = 0; i < numVertices; i++)
//             {

//                 if ((distancias[i] == peso) && !verticeJaVisitado[i])
//                 {

//                     for (int j = 0; j < numVertices; j++)
//                     {

//                         if ((adjMatrixDouble[i][j] < Double.POSITIVE_INFINITY)
//                                 && verticeJaVisitado[j])
//                         {
//                             return i; //devolve o index
//                         }
//                     }
//                 }
//             }
//             // nunca chega aqui o método, só em caso de não ter encontrado o vértice, que nunca acontece
//             return -1;
//         }

//         /**
//          *
//          * Retorna o peso do caminho mais curto dados dois utilizadores ou infinito
//          * se não foi encontrado pelo menos um caminho
//          *
//          * @param indiceUtilizador1
//          * @param indiceUtilizador2
//          * @return
//          */
//         public double pesoCaminhoMaisCurto(int indiceUtilizador1, int indiceUtilizador2)
//         {
//             double result = 0;

//             //verifica se os utilizadores são válidos
//             if (!indexIsValid(indiceUtilizador1) || !indexIsValid(indiceUtilizador2))
//             {
//                 return Double.POSITIVE_INFINITY;
//             }

//             int index1, index2;

//             // vê qual o caminho mais curto
//             Iterator<T> it = caminhoMaisCurto(indiceUtilizador1, indiceUtilizador2);

//             if (it.hasNext())
//             {
//                 index1 = verificaPessoa(it.next());

//             }
//             else
//             {
//                 // retorna infinito se não existir pelo menos um caminho
//                 return Double.POSITIVE_INFINITY;
//             }

//             while (it.hasNext())
//             {
//                 index2 = verificaPessoa(it.next());

//                 //calculo do peso do caminho
//                 result += adjMatrixDouble[index1][index2];
//                 index1 = index2;
//             }

//             return result; //retorna o peso do caminho
//         }

//         /**
//          *
//          * Retorna o peso do caminho mais curto dados dois utilizadores ou infinito
//          * se não foi encontrado pelo menos um caminho
//          *
//          * @param indiceUtilizador1
//          * @param indiceUtilizador2
//          * @return
//          */
//         public double pesoCaminhoMaisLongo(int indiceUtilizador1, int indiceUtilizador2)
//         {
//             double result = 0;

//             //verifica se os utilizadores são válidos
//             if (!indexIsValid(indiceUtilizador1) || !indexIsValid(indiceUtilizador2))
//             {
//                 return Double.POSITIVE_INFINITY;
//             }

//             int index1, index2;

//             // vê qual o caminho mais longo
//             Iterator<T> it = caminhoMaisLongo(indiceUtilizador1, indiceUtilizador2);

//             if (it.hasNext())
//             {
//                 index1 = verificaPessoa(it.next());

//             }
//             else
//             {
//                 // retorna infinito se não existir pelo menos um caminho
//                 return Double.POSITIVE_INFINITY;
//             }

//             while (it.hasNext())
//             {
//                 index2 = verificaPessoa(it.next());

//                 //calculo do peso do caminho
//                 result += adjMatrixDouble[index1][index2];
//                 index1 = index2;
//             }

//             return result; //retorna o peso do caminho
//         }

//         /**
//          *
//          * Retorna o peso do caminho mais curto dados dois utilizadores ou infinito
//          * se não foi encontrado pelo menos um caminho
//          *
//          * @param indiceUtilizador1
//          * @param indiceUtilizador2
//          * @return
//          */
//         public double pesoCaminhoMaisCurtoMencoes(int indiceUtilizador1, int indiceUtilizador2)
//         {
//             double result = 0;

//             //verifica se os utilizadores são válidos
//             if (!indexIsValid(indiceUtilizador1) || !indexIsValid(indiceUtilizador2))
//             {
//                 return Double.POSITIVE_INFINITY;
//             }

//             int index1, index2;

//             // vê qual o caminho mais curto
//             Iterator<T> it = caminhoMaisCurtoMencoes(indiceUtilizador1, indiceUtilizador2);

//             if (it.hasNext())
//             {
//                 index1 = verificaPessoa(it.next());

//             }
//             else
//             {
//                 // retorna infinito se não existir pelo menos um caminho
//                 return Double.POSITIVE_INFINITY;
//             }

//             while (it.hasNext())
//             {
//                 index2 = verificaPessoa(it.next());

//                 //calculo do peso do caminho
//                 result += adjMatrixDouble[index1][index2];
//                 index1 = index2;
//             }

//             return result; //retorna o peso do caminho
//         }

//         @Override
//         public Iterator<T> listarConhecidos(int indiceUtilizador)
//         {

//             SharedObj utilizadoresAlcancaveis = new SharedObj();
//             utilizadoresAlcancaveis.getConhecidos().addToRear((Pessoa)this.vertices[indiceUtilizador]);

//             for (int i = 0; i < this.numVertices; i++)
//             {

//                 if (indiceUtilizador != i && this.adjMatrixDouble[indiceUtilizador][i] < Double.POSITIVE_INFINITY)
//                 {
//                     utilizadoresAlcancaveis.getConhecidos().addToRear((Pessoa)this.vertices[i]);
//                 }
//             }

//             ArrayUnorderedList<Pessoa> conhece = utilizadoresAlcancaveis.getConhecidos();

//             for (Pessoa l : conhece)
//             {
//                 listarConhecidosAlcancaveis(utilizadoresAlcancaveis, l);
//             }

//             return (Iterator<T>)utilizadoresAlcancaveis.getConhecidos().iterator();

//             //ou esta solução mais simples usando o iterator BFS com o parametro inicial do indice de origem
//             //return this.iteratorBFS(indiceUtilizador);
//         }

//         @Override
//         public Iterator<T> listarConhecidosViaUmIntermediario(int indiceUtilizador)
//         {

//             //Criar um objeto partilhado com os conhecidos
//             SharedObj<T> utilizadoresUmIntermediario = new SharedObj();

//             //Adicionar o próprio utilizador ao objeto partilhado
//             utilizadoresUmIntermediario.getConhecidos().addToRear((Pessoa)this.vertices[indiceUtilizador]);

//             //Adicionar os conhecidos do próprio utilizador ao objeto partilhado
//             for (int i = 0; i < this.numVertices; i++)
//             {

//                 if (indiceUtilizador != i && this.adjMatrixDouble[indiceUtilizador][i] < Double.POSITIVE_INFINITY && !utilizadoresUmIntermediario.getConhecidos().contains(this.vertices[i]))
//                 {
//                     utilizadoresUmIntermediario.getConhecidos().addToRear((Pessoa)this.vertices[i]);
//                 }
//             }

//             ArrayUnorderedList<Integer> conhece = new ArrayUnorderedList<>();

//             for (T p : utilizadoresUmIntermediario.getConhecidos())
//             {
//                 conhece.addToRear(verificaPessoa(p));
//             }

//             //Para cada contacto direto, verifica os seus contactos
//             for (int indice : conhece)
//             {
//                 if (indice != indiceUtilizador)
//                 {
//                     listarConhecidosAlcancaveisViaUmIntermediario(utilizadoresUmIntermediario, indice);
//                 }
//             }

//             return (Iterator<T>)utilizadoresUmIntermediario.getConhecidos().iterator();
//         }

//         /**
//          * metodo para saber os conhecidos dos conhecidos
//          *
//          * @param utilizadoresAlcancaveis
//          * @param indice
//          */
//         public void listarConhecidosAlcancaveisViaUmIntermediario(SharedObj utilizadoresAlcancaveis, int indice)
//         {

//             //Para cada contacto do utilizador de origem, adiciona os contactos destes
//             for (int contactoDeUmConhecido : ((Pessoa)this.vertices[indice]).getContacts())
//             {

//                 if (!utilizadoresAlcancaveis.getConhecidos().contains((Pessoa)this.vertices[(int)contactoDeUmConhecido]))
//                 {

//                     utilizadoresAlcancaveis.getConhecidos().addToRear((Pessoa)this.vertices[(int)contactoDeUmConhecido]);
//                 }
//             }
//         }

//         /**
//          * metodo recursivo para saber os conhecidos dos conhecidos
//          *
//          * @param utilizadoresAlcancaveis
//          * @param user
//          */
//         public void listarConhecidosAlcancaveis(SharedObj utilizadoresAlcancaveis, Pessoa user)
//         {

//             boolean jaConheceuTodos = true;
//             ArrayUnorderedList<Integer> aindaNaoPesquisou = new ArrayUnorderedList<>();

//             for (int contactoDeUmConhecido : user.getContacts())
//             {
//                 if (!utilizadoresAlcancaveis.getConhecidos().contains((Pessoa)this.vertices[(int)contactoDeUmConhecido]))
//                 {
//                     utilizadoresAlcancaveis.getConhecidos().addToRear((Pessoa)this.vertices[(int)contactoDeUmConhecido]);
//                     aindaNaoPesquisou.addToRear((int)contactoDeUmConhecido);
//                     jaConheceuTodos = false;
//                 }
//             }

//             if (!jaConheceuTodos)
//             {
//                 for (int i : aindaNaoPesquisou)
//                 {
//                     listarConhecidosAlcancaveis(utilizadoresAlcancaveis, (Pessoa)this.vertices[i]);
//                 }
//             }
//         }

//         @Override
//         public Iterator<T> listarDesconhecidos(int indiceUtilizador)
//         {
//             ArrayUnorderedList<T> todasPessoas = new ArrayUnorderedList<>();

//             //adiciona todos os vertices
//             for (T pessoa : this.vertices)
//             {
//                 todasPessoas.addToRear(pessoa);
//             }

//             //para ter todos os conhecidos
//             Iterator<T> it = listarConhecidos(indiceUtilizador);

//             //remove todos os conhecidos
//             while (it.hasNext())
//             {
//                 todasPessoas.remove(it.next());
//             }

//             return (Iterator<T>)todasPessoas.iterator();
//         }

//         @Override
//         public T editarLigacoes(int indiceUtilizador1, int indiceUtilizador2)
//         {
//             T pessoaAEditar = this.vertices[indiceUtilizador1];

//             //existe ligação
//             if (this.adjMatrixDouble[indiceUtilizador1][indiceUtilizador2] < Double.POSITIVE_INFINITY)
//             {

//                 //remover a ligação nos vertices
//                 ((Pessoa)this.vertices[indiceUtilizador1]).getContacts().remove(indiceUtilizador2);
//                 ((Pessoa)this.vertices[indiceUtilizador2]).getContacts().remove(indiceUtilizador1);

//                 //remover a ligação na matriz
//                 this.adjMatrixDouble[indiceUtilizador1][indiceUtilizador2] = Double.POSITIVE_INFINITY;
//                 this.adjMatrixDouble[indiceUtilizador2][indiceUtilizador1] = Double.POSITIVE_INFINITY;

//             }
//             else
//             { //não existe ligação

//                 //adicionar a ligação nos vertices
//                 ((Pessoa)this.vertices[indiceUtilizador1]).getContacts().add(indiceUtilizador2);
//                 ((Pessoa)this.vertices[indiceUtilizador2]).getContacts().add(indiceUtilizador1);

//                 //adicionar a ligação à matriz
//                 this.adjMatrixDouble[indiceUtilizador1][indiceUtilizador2] = (1.0 / ((Pessoa)this.vertices[indiceUtilizador2]).getVisualizacoes());
//                 this.adjMatrixDouble[indiceUtilizador2][indiceUtilizador1] = (1.0 / ((Pessoa)this.vertices[indiceUtilizador1]).getVisualizacoes());
//             }

//             return pessoaAEditar;
//         }

//         @Override
//         public T editarVisualizacoes(int indiceUtilizador, int visualizacoes)
//         {
//             T pessoaAEditar = this.vertices[indiceUtilizador];
//             ((Pessoa)pessoaAEditar).setVisualizacoes(visualizacoes);

//             for (int l : ((Pessoa)pessoaAEditar).getContacts())
//             {
//                 adjMatrixDouble[(int)l][indiceUtilizador] = (1.0 / ((Pessoa)pessoaAEditar).getVisualizacoes());
//             }

//             return pessoaAEditar;
//         }

//         @Override
//         public Iterator<T> listagemDeConhecidosComSkillsUmIntermediario(int indiceUtilizador, ArrayUnorderedList<String> skills)
//         {

//             ArrayUnorderedList<T> relacionadosSkills = new ArrayUnorderedList<>();
//             Iterator<T> it = this.listarConhecidosViaUmIntermediario(indiceUtilizador);

//             boolean skillExiste;
//             int contador;
//             Pessoa pessoa;

//             //Para verificar se existem conhecidos
//             if (it.hasNext())
//             {
//                 it.next(); //Para não verificar o próprio utilizador

//                 //Para cada conhecido
//                 while (it.hasNext())
//                 {
//                     pessoa = (Pessoa)it.next();

//                     //Para verificar se o utilizador tem todas as skills recebidas
//                     contador = skills.size();

//                     //Para cada skill
//                     for (String skillRecebida : skills)
//                     {
//                         skillExiste = false;

//                         //Verificar se o utilizador possui esta skill
//                         for (String skillUtilizador : pessoa.getSkills())
//                         {

//                             if (skillRecebida.equalsIgnoreCase(skillUtilizador))
//                             {
//                                 skillExiste = true;
//                                 contador--;
//                                 break;
//                             }
//                         }

//                         //Se não existe uma skill, sai deste utilizador
//                         if (!skillExiste)
//                         {
//                             break;
//                         }
//                     }

//                     //Adicionar o utilizador que corresponde à procura
//                     if (contador == 0)
//                     {
//                         relacionadosSkills.addToRear(pessoa);
//                     }
//                 }
//             }

//             return relacionadosSkills.iterator();
//         }

//         @Override
//         public Iterator<T> listagemDeConhecidosComEmpresaUmIntermediario(int indiceUtilizador, String empresa)
//         {
//             ArrayUnorderedList<T> relacionadosEmpresa = new ArrayUnorderedList<>();
//             Iterator<T> it = this.listarConhecidosViaUmIntermediario(indiceUtilizador);

//             Pessoa pessoa;

//             //Para verificar se existem conhecidos
//             if (it.hasNext())
//             {
//                 it.next(); //Para não verificar o próprio utilizador

//                 //Para cada conhecido
//                 while (it.hasNext())
//                 {
//                     pessoa = (Pessoa)it.next();

//                     for (CargosProfissionais cp : pessoa.getCargosProfissionais())
//                     {
//                         if (empresa.equalsIgnoreCase(cp.getEmpresa())) //Adicionar o utilizador que corresponde à procura
//                         {
//                             relacionadosEmpresa.addToRear(pessoa);
//                         }
//                     }
//                 }
//             }

//             return relacionadosEmpresa.iterator();
//         }

//         @Override
//         public Iterator<T> pesquisaDeConhecidosComEmpresa(int indiceUtilizador, String empresa)
//         {
//             ArrayUnorderedList<T> relacionados = new ArrayUnorderedList<>();
//             Pessoa pessoa;

//             Iterator<T> it = listarConhecidos(indiceUtilizador);
//             while (it.hasNext())
//             {

//                 for (CargosProfissionais cp : (pessoa = (Pessoa)it.next()).getCargosProfissionais())
//                 {

//                     if (cp.getEmpresa().equalsIgnoreCase(empresa))
//                     {
//                         relacionados.addToRear(pessoa);
//                     }
//                 }
//             }

//             return relacionados.iterator();
//         }

//         @Override
//         public Iterator<T> utilizadoresNaoRelacionadosPorEmpresa(String empresa1, String empresa2)
//         {
//             ArrayUnorderedList<T> funcionariosA = new ArrayUnorderedList<>();
//             ArrayUnorderedList<T> funcionariosB = new ArrayUnorderedList<>();
//             ArrayUnorderedList<T> funcionariosNaoRelacionados = new ArrayUnorderedList<>();

//             //Inserir todos os funcionarios da empresa1 ao array funcionariosA
//             for (T p : this.vertices)
//             {

//                 for (CargosProfissionais cp : ((Pessoa)p).getCargosProfissionais())
//                 {

//                     if (cp.getEmpresa().equalsIgnoreCase(empresa1))
//                     {
//                         funcionariosA.addToRear(p);
//                     }
//                 }
//             }

//             //Inserir todos os funcionarios da empresa2  ao array funcionariosB
//             for (T p : this.vertices)
//             {

//                 for (CargosProfissionais cp : ((Pessoa)p).getCargosProfissionais())
//                 {

//                     if (cp.getEmpresa().equalsIgnoreCase(empresa2))
//                     {
//                         funcionariosB.addToRear(p);
//                     }
//                 }
//             }

//             //Para cada funcionário da empresa 1, se não existe nenhum contacto na empresa 2 então adiciona ao array de utilizadores não relacionados
//             for (T funcionario : funcionariosA)
//             {
//                 int contador = ((Pessoa)funcionario).getContacts().size();

//                 for (int contacto : ((Pessoa)funcionario).getContacts())
//                 {

//                     if (!funcionariosB.contains(this.vertices[(int)contacto]) && !funcionariosB.contains(funcionario) && !funcionariosNaoRelacionados.contains(funcionario))
//                     {
//                         contador--;
//                     }
//                     else
//                     {
//                         break;
//                     }
//                 }

//                 if (contador == 0)
//                 {
//                     funcionariosNaoRelacionados.addToRear(funcionario);
//                 }

//             }

//             //Para cada funcionário da empresa 2, se não existe nenhum contacto na empresa 1 então adiciona ao array de utilizadores não relacionados
//             for (T funcionario : funcionariosB)
//             {

//                 int contador = ((Pessoa)funcionario).getContacts().size();

//                 for (int contacto : ((Pessoa)funcionario).getContacts())
//                 {

//                     if (!funcionariosA.contains(this.vertices[(int)contacto]) && !funcionariosA.contains(funcionario) && !funcionariosNaoRelacionados.contains(funcionario))
//                     {
//                         contador--;
//                     }
//                     else
//                     {
//                         break;
//                     }
//                 }

//                 if (contador == 0)
//                 { //funcionario não relacionado entre as empresas
//                     funcionariosNaoRelacionados.addToRear(funcionario);
//                 }

//             }

//             return funcionariosNaoRelacionados.iterator();
//         }

//         @Override
//         public Iterator<PessoaConhecidaComSkillPeso> pesquisaDeUtilizadoresPorSkillOrdenado(int indiceUtilizador, String skill)
//         {
//             ArrayUnorderedList<T> relacionadosSkill = new ArrayUnorderedList<>();
//             Iterator<T> it = this.listarConhecidos(indiceUtilizador);
//             LinkedHeap<PessoaConhecidaComSkillPeso> minHeap = new LinkedHeap<>();
//             ArrayUnorderedList<PessoaConhecidaComSkillPeso> resultadoOrdenado = new ArrayUnorderedList<>();
//             T pessoa;
//             PessoaConhecidaComSkillPeso<T> pessoaConhecidaComSkillPeso = null;

//             //Para verificar se existem conhecidos
//             if (it.hasNext())
//             {
//                 it.next(); //Para não verificar o próprio utilizador

//                 //Para cada conhecido
//                 while (it.hasNext())
//                 {
//                     pessoa = it.next();

//                     for (String s : ((Pessoa)pessoa).getSkills())
//                     {
//                         if (skill.equalsIgnoreCase(s)) //Adicionar o utilizador que corresponde à procura
//                         {
//                             relacionadosSkill.addToRear(pessoa);
//                         }
//                     }
//                 }
//             }

//             /**
//              * Para cada skill da pessoa verifica se existe a skill pretendida
//              */
//             for (T p : relacionadosSkill)
//             {
//                 pessoaConhecidaComSkillPeso = new PessoaConhecidaComSkillPeso();
//                 pessoaConhecidaComSkillPeso.setPesoParaEstaPessoa(pesoCaminhoMaisCurto(indiceUtilizador, verificaPessoa(p)));
//                 pessoaConhecidaComSkillPeso.setPessoa(p);
//                 minHeap.addElement(pessoaConhecidaComSkillPeso);
//             }

//             resultadoOrdenado.addToFront(minHeap.removeMin());

//             while (!minHeap.isEmpty())
//             {
//                 resultadoOrdenado.addToRear(minHeap.removeMin());
//             }

//             return resultadoOrdenado.iterator();
//         }

//         /**
//          * Retorna uma representação da network
//          */
//         @Override
//         public String toString()
//         {

//             DecimalFormat df = new DecimalFormat("0.000"); //3 casas decimais

//             if (numVertices == 0)
//             {
//                 return "Grafo vazio";
//             }

//             String result = "";

//             /**
//              * Imprime matriz de adjacencias
//              */
//             result += "Adjacency Matrix\n";
//             result += "----------------\n";
//             result += "Index";

//             for (int i = 0; i < numVertices; i++)
//             {
//                 result += "    " + i;
//                 if (i < 10)
//                 {
//                     result += " ";
//                 }
//             }
//             result += "\n\n";

//             for (int i = 0; i < numVertices; i++)
//             {
//                 result += "" + i + "\t ";

//                 for (int j = 0; j < numVertices; j++)
//                 {
//                     if (adjMatrixDouble[i][j] < Double.POSITIVE_INFINITY)
//                     {
//                         result += (df.format(adjMatrixDouble[i][j])) + " ";
//                     }
//                     else
//                     {
//                         result += "0,000 ";
//                     }
//                 }
//                 result += "\n";
//             }

//             /**
//              * Imprime valor dos vértices
//              */
//             result += "\n\nVertices";
//             result += "\n-------------\n";
//             result += "Indice\tValor\n\n";

//             for (int i = 0; i < numVertices; i++)
//             {
//                 result += "" + i + "\t";
//                 result += vertices[i].toString() + "\n";
//             }

//             /**
//              * Imprime pesos das arestas
//              */
//             result += "\n\nPesos das arestas";
//             result += "\n----------------\n";
//             result += "Indice\tPeso\n\n";

//             for (int i = 0; i < numVertices; i++)
//             {
//                 for (int j = 0; j < numVertices; j++)
//                 {
//                     if (j != i && adjMatrixDouble[i][j] != Double.POSITIVE_INFINITY)
//                     {
//                         result += i + " para " + j + "\t";
//                         result += df.format(adjMatrixDouble[i][j]) + "\n";
//                     }
//                 }
//             }

//             result += "\n";
//             return result;
//         }

//         /**
//          * verifica se a pessoa existe retornando a sua posição ou -1 em caso
//          * negativo
//          *
//          * @param email
//          * @return
//          */
//         public int verificarEmail(String email)
//         {

//             for (int i = 0; i < this.numVertices; i++)
//             {
//                 if (((Pessoa)this.vertices[i]).getEmail().equalsIgnoreCase(email))
//                 {
//                     return i;
//                 }
//             }

//             return -1;
//         }

//         /**
//          * verifica se a pessoa existe retornando a sua posição ou -1 em caso
//          * negativo
//          *
//          * @param pessoa
//          * @return
//          */
//         private int verificaPessoa(T pessoa)
//         {

//             for (int i = 0; i < this.numVertices; i++)
//             {
//                 if (this.vertices[i] == pessoa)
//                 {
//                     return i;
//                 }
//             }

//             return -1;
//         }

//         /**
//          * método para expandir a matriz e o array de vertices caso a network esteja
//          * cheia ao adicionar um novo elemento
//          */
//         private void expandCapacity()
//         {
//             T[] largerVertices = (T[])(new Object[vertices.length * 2]);

//             double[][] largerAdjMatrix
//                     = new double[vertices.length * 2][vertices.length * 2];

//             for (int i = 0; i < numVertices; i++)
//             {

//                 for (int j = 0; j < numVertices; j++)
//                 {
//                     largerAdjMatrix[i][j] = adjMatrixDouble[i][j];
//                 }

//                 largerVertices[i] = vertices[i];
//             }

//             vertices = largerVertices;
//             adjMatrixDouble = largerAdjMatrix;
//         }

//         /**
//          * Returns an iterator that performs a breadth first search traversal
//          * starting at the given index.
//          *
//          * @param t
//          * @return an iterator that performs a breadth first traversal
//          */
//         @Override
//         public Iterator<T> iteratorBFS(T t)
//         {
//             Integer x;
//             LinkedQueue<Integer> traversalQueue = new LinkedQueue<>();
//             ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
//             Integer startIndex = getIndex(t);

//             if (!indexIsValid(startIndex))
//             {
//                 return resultList.iterator();
//             }
//             boolean[] visited = new boolean[numVertices];
//             for (int i = 0; i < numVertices; i++)
//             {
//                 visited[i] = false;
//             }

//             traversalQueue.enqueue(new Integer(startIndex));
//             visited[startIndex] = true;
//             while (!traversalQueue.isEmpty())
//             {
//                 x = traversalQueue.dequeue();
//                 resultList.addToRear(vertices[x.intValue()]);
//                 /**
//                  * Find all vertices adjacent to x that have not been visited and
//                  * queue them up
//                  */
//                 for (int i = 0; i < numVertices; i++)
//                 {
//                     if (adjMatrixDouble[x.intValue()][i] < Double.POSITIVE_INFINITY && !visited[i])
//                     {
//                         traversalQueue.enqueue(new Integer(i));
//                         visited[i] = true;
//                     }
//                 }
//             }
//             return resultList.iterator();
//         }

//         /**
//          * Returns an iterator that performs a breadth first search traversal
//          * starting at the given index.
//          *     
//         * @param startIndex the index to begin the search from
//          * @return an iterator that performs a breadth first traversal
//          */
//         public Iterator<T> iteratorBFS(int startIndex)
//         {
//             Integer x;
//             LinkedQueue<Integer> traversalQueue = new LinkedQueue<Integer>();
//             ArrayUnorderedList<T> resultList = new ArrayUnorderedList<T>();
//             if (!indexIsValid(startIndex))
//             {
//                 return resultList.iterator();
//             }
//             boolean[] visited = new boolean[numVertices];
//             for (int i = 0; i < numVertices; i++)
//             {
//                 visited[i] = false;
//             }
//             traversalQueue.enqueue(new Integer(startIndex));
//             visited[startIndex] = true;
//             while (!traversalQueue.isEmpty())
//             {
//                 x = traversalQueue.dequeue();
//                 resultList.addToRear(vertices[x.intValue()]);
//                 /**
//                  * Find all vertices adjacent to x that have not been visited and
//                  * queue them up
//                  */
//                 for (int i = 0; i < numVertices; i++)
//                 {
//                     if (adjMatrixDouble[x.intValue()][i] < Double.POSITIVE_INFINITY && !visited[i])
//                     {
//                         traversalQueue.enqueue(new Integer(i));
//                         visited[i] = true;
//                     }
//                 }
//             }
//             return resultList.iterator();
//         }

//         /**
//          * Returns an iterator that performs a depth first search traversal starting
//          * at the given index.
//          *
//          * @param t the index to begin the search traversal from
//          * @return an iterator that performs a depth first traversal
//          */
//         @Override
//         public Iterator<T> iteratorDFS(T t)
//         {
//             Integer x;
//             boolean found;
//             LinkedStack<Integer> traversalStack = new LinkedStack<>();
//             ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
//             boolean[] visited = new boolean[numVertices];
//             Integer startIndex = getIndex(t);

//             if (!indexIsValid(startIndex))
//             {
//                 return resultList.iterator();
//             }
//             for (int i = 0; i < numVertices; i++)
//             {
//                 visited[i] = false;
//             }

//             traversalStack.push(new Integer(startIndex));
//             resultList.addToRear(vertices[startIndex]);
//             visited[startIndex] = true;

//             while (!traversalStack.isEmpty())
//             {
//                 x = traversalStack.peek();
//                 found = false;
//                 /**
//                  * Find a vertex adjacent to x that has not been visited and push it
//                  * on the stack
//                  */
//                 for (int i = 0; (i < numVertices) && !found; i++)
//                 {
//                     if (adjMatrixDouble[x.intValue()][i] < Double.POSITIVE_INFINITY && !visited[i])
//                     {
//                         traversalStack.push(new Integer(i));
//                         resultList.addToRear(vertices[i]);
//                         visited[i] = true;
//                         found = true;
//                     }
//                 }
//                 if (!found && !traversalStack.isEmpty())
//                 {
//                     traversalStack.pop();
//                 }
//             }
//             return resultList.iterator();
//         }

//         /**
//          * Returns an iterator that performs a depth first search traversal starting
//          * at the given index.
//          *     
//         * @param startIndex the index to begin the search traversal from
//          * @return an iterator that performs a depth first traversal
//          */
//         public Iterator<T> iteratorDFS(int startIndex)
//         {
//             Integer x;
//             boolean found;
//             LinkedStack<Integer> traversalStack = new LinkedStack<>();
//             ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
//             boolean[] visited = new boolean[numVertices];
//             if (!indexIsValid(startIndex))
//             {
//                 return resultList.iterator();
//             }
//             for (int i = 0; i < numVertices; i++)
//             {
//                 visited[i] = false;
//             }
//             traversalStack.push(new Integer(startIndex));
//             resultList.addToRear(vertices[startIndex]);
//             visited[startIndex] = true;
//             while (!traversalStack.isEmpty())
//             {
//                 x = traversalStack.peek();
//                 found = false;
//                 /**
//                  * Find a vertex adjacent to x that has not been visited and push it
//                  * on the stack
//                  */
//                 for (int i = 0; (i < numVertices) && !found; i++)
//                 {
//                     if (adjMatrixDouble[x.intValue()][i] < Double.POSITIVE_INFINITY && !visited[i])
//                     {
//                         traversalStack.push(new Integer(i));
//                         resultList.addToRear(vertices[i]);
//                         visited[i] = true;
//                         found = true;
//                     }
//                 }
//                 if (!found && !traversalStack.isEmpty())
//                 {
//                     traversalStack.pop();
//                 }
//             }
//             return resultList.iterator();
//         }
//     }
// }