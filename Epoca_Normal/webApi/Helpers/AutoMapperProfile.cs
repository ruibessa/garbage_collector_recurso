using AutoMapper;
using WebApi.Entities;
using WebApi.Models.Users;
using WebApi.Models.Ecoponto;
using WebApi.Models.Notificacao;
using WebApi.Models.Empresa;
using WebApi.Models.Localidade;

namespace WebApi.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<User, UserModel>();
            CreateMap<UserModel,User>();
            CreateMap<RegisterModel, User>();
            CreateMap<EmpresaRegisterModel, Empresa>();
            CreateMap<EmpresaRegisterModel, User>();
            CreateMap<UpdateModel, User>();
            CreateMap<Ecoponto, EcopontoModel>();
            CreateMap<CriarEcoponto, Ecoponto>();
            CreateMap<AtualizarEcoponto, Ecoponto>();
            CreateMap<Notificacao, NotificacaoModel>();
            CreateMap<CriarNotificacao, Notificacao>();
            CreateMap<AtualizarNotificacao, Notificacao>();
            CreateMap<CamiaoRegisterModel, Camiao>();
            CreateMap<LocalidadeModel, Localidade>();
        }
    }
}