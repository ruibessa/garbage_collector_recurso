import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { User, Role } from '../_models';
import { UserService, AuthenticationService } from '../_services';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.less']
})
export class ProfileComponent implements OnInit {
  currentUser: User;
  users = [];
  loading = false;
  userFromApi: User;
  role: boolean = false;
  

  constructor(
    private authenticationService: AuthenticationService,
    private userService: UserService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;

    console.log(this.currentUser);

    if (this.currentUser.role == Role.Admin) {
      this.role = true;
    };
  }

  ngOnInit() {
    this.loadAllUsers();
    this.loading = true;
    this.userService.getById(this.currentUser.id).pipe(first()).subscribe(user => {
      this.loading = false;
      this.userFromApi = user;
    });
  }

  private loadAllUsers() {
    this.userService.getAll()
      .pipe(first())
      .subscribe(users => this.users = users);
  }

}
