import {Role} from "./role"

export class Empresa{
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
    role: string;
    nif: string;
    nomeEmpresa: string;
    localizacaoempresa: string;
    token?: string;
}