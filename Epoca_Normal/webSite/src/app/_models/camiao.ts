import { TipoCamiao } from './tipoCamiao';
import { EstadoCamiao } from "./estadoCamiao";

export class Camiao {
    id: number;
    matricula: string;
    carga: number;
    distrito: string;
    concelho: string;
    tipo: string;
    empresa: string;
    estado: string;
    
}