import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagensUsersComponent } from './listagens-users.component';

describe('ListagensUsersComponent', () => {
  let component: ListagensUsersComponent;
  let fixture: ComponentFixture<ListagensUsersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListagensUsersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListagensUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
