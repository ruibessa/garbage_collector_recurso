import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListagemNotificacaoComponent } from './listagem-notificacoes.component';

describe('NotificacaoComponent', () => {
  let component: ListagemNotificacaoComponent;
  let fixture: ComponentFixture<ListagemNotificacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListagemNotificacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListagemNotificacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
