import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

// import fade in animation
import { fadeInAnimation } from '../_animations/index';
import { PubSubService, CamiaoService, AuthenticationService } from '@app/_services';

@Component({

  templateUrl: './camiao.component.html',
  styleUrls: ['./camiao.component.less'],
  // make fade in animation available to this component
  animations: [fadeInAnimation],

  // attach the fade in animation to the host (root) element of this component
  host: { '[@fadeInAnimation]': '' }
})
export class CamiaoComponent implements OnInit {
  camioes: any[];
  subscription: Subscription;
  loading = false;
  userId: number;

  constructor(
    private camiaoService: CamiaoService,
    private pubSubService: PubSubService,
    private authenticationService: AuthenticationService
  ) { 
    this.userId = this.authenticationService.currentUserValue.id;

  }

  ngOnInit() {
    this.loadCamioes();

    // reload camioes when updated
    this.subscription = this.pubSubService.on('camioes-updated').subscribe(() => this.loadCamioes());
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

  deleteCamiao(id: number) {
    this.camioes.find(x => x.id === id).deleting = true;
    this.camiaoService.delete(id).subscribe(() => {
      // remove camiao from camioes array after deleting
      this.camioes = this.camioes.filter(x => x.id !== id);
    });
  }

  private loadCamioes() {
    this.camiaoService.getAll(this.userId).subscribe(x => this.camioes = x);
  }



}
