﻿import { environment } from './../../environments/environment';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, UserService, AuthenticationService, NotificationService  } from '../_services';
import { User, Role } from '../_models';
import { HttpEventType, HttpClient } from '@angular/common/http';

@Component({ 
    selector: "Notification",
    templateUrl: 'notification.component.html' 
})
export class NotificationComponent implements OnInit {
    notificationForm: FormGroup;
    loading = false;
    submitted = false;
    fileData: File = null;
    previewUrl: any = null;
    fileUploadProgress: string = null;
    uploadedFilePath: string = null;
currentUserId: number;
    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private alertService: AlertService,
        private notificationService : NotificationService,
        private http: HttpClient
            ) {
        
    }

    ngOnInit() {
        this.notificationForm = this.formBuilder.group({
            descricao: ['', Validators.required],
            imagem: ['',Validators.required]
        });
            environment.tipoMapa = 2
    }

    // convenience getter for easy access to form fields
    get f() { return this.notificationForm.controls; }

  fileProgress(fileInput: any) {
    this.fileData = <File>fileInput.target.files[0];
    this.preview();
  }

  preview() {
    // Show preview
    var mimeType = this.fileData.type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(this.fileData);
    reader.onload = _event => {
      this.previewUrl = reader.result;
      environment.imageUrl = this.previewUrl;
    };
  }

  onSubmit() {
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    if (this.notificationForm.invalid) {
        return;
    }

    this.loading = true; 
this.currentUserId = this.authenticationService.currentUserValue.id;
    this.notificationService.register(this.notificationForm.value, this.currentUserId)
        .pipe(first())
        .subscribe(
            data => {
                this.alertService.success('Notification successful sended', true);
                this.router.navigate(['/home']);
            },
            error => {
                this.alertService.error(error);
                this.loading = false;
            });
}
}