import socket
import sys
import time

sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
host = '172.20.129.178' 
port = 11000 
server_address = (host, port)
sock.connect(server_address)
try:
    number = 10
    while True:
        message = str(number)
        mess = bytes(message, encoding='ascii')
        time.sleep(4)
        number = number + 1
        sock.sendall(mess)
        print(mess)
finally:
    sock.close()
