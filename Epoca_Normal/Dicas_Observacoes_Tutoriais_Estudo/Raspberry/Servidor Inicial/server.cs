using System.Net.Sockets;
using System.Net;
using System;

class Server {
    public static void Main() {
        string ip = "172.20.129.178";
        int port = 11000;
        TcpListener server = new TcpListener(IPAddress.Parse(ip), port);
        Console.WriteLine("Servidor ligado no ip:porta " + ip + ":" + port);
        Console.WriteLine("A espera do cliente");
        server.Start();
        TcpClient client = server.AcceptTcpClient();
        Console.WriteLine("Raspberry conectado");
        NetworkStream stream = client.GetStream();
        while (true) {
            while (!stream.DataAvailable);
            Byte[] bytes = new Byte[client.Available];
            int inteiro = stream.Read(bytes, 0, bytes.Length);
            String data = null;
            data = System.Text.Encoding.ASCII.GetString(bytes, 0, inteiro);
            Console.WriteLine("Valor: {0}", data);
        }
    }
}
