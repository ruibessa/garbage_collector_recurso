﻿import { TipoEcoponto } from './tipoEcoponto';
import { EstadoEcoponto } from "./estadoEcoponto";

export class Ecoponto {
    id: number;
    sensor: string;
    distrito: string;
    concelho: string;
    freguesia: string;
    rua: string;
    codigoPostal: string;
    lat: number;
    lon: number;
    tipo: string;
    altura: number;
    alturaLivre: number;
    capacidade: number;
    estado: number;
}