import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Empresa } from '../_models';

@Injectable({ providedIn: 'root' })
export class EmpresaService {
    constructor(private http: HttpClient) { }

    registerEmpresa(empresa: Empresa) {
        console.log("Adicionar" + empresa);
        return this.http.post(`${environment.apiUrl}/Empresa/register`, empresa);
    }
}