import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { AlertService, CamiaoService, PubSubService, AuthenticationService } from '../_services/index';

// import slide in/out animation
import { slideInOutAnimation } from '../_animations/index';
import { User} from '../_models';

@Component({
    templateUrl: 'camiao-add-edit.component.html',

    // make slide in/out animation available to this component
    animations: [slideInOutAnimation],

    // attach the slide in/out animation to the host (root) element of this component
    host: { '[@slideInOutAnimation]': '' }
})
export class CamiaoAddEditComponent implements OnInit {
    title: string;
    camiao: any = {};
    saving = false;
    currentUserId: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private camiaoService: CamiaoService,
        private pubSubService: PubSubService,
        private alertService: AlertService,
        private authenticationService: AuthenticationService
    ) { 
        this.currentUserId = this.authenticationService.currentUserValue.id + "";
    }

    ngOnInit() {
        
        this.title = 'Add Camiao';
        const camiaoId = Number(this.route.snapshot.params['id']);
        if (camiaoId) {
            this.title = 'Edit Camiao';
            this.camiaoService.getById(camiaoId).subscribe(x => this.camiao = x);
        }
    }

    saveCamiao() {
        // save camiao
        this.saving = true;
        this.alertService.clear();
        const action = this.camiao.id ? 'update' : 'create';
        
        this.camiaoService[action](this.camiao, this.currentUserId)
            .subscribe(
                data => {
                    this.saving = true;
                    this.alertService.success('Registration successful', true);

                    // redirect to camioes view
                    this.router.navigate(['camioes']);

                    // publish event so list component refreshes
                    this.pubSubService.publish('camioes-updated');
                },
                error => {
                    this.alertService.error(error);
                    this.saving = false;
                }
            );
    }
}