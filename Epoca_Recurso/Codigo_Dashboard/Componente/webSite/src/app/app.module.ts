import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// used to create fake backend
import { fakeBackendProvider } from './_helpers';

import { appRoutingModule } from './app.routing';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AppComponent } from './app.component';
import { HomeComponent } from './home';
import { AdminComponent } from './admin';
import { RegisterComponent } from './register';
import { LoginComponent } from './login/login.component';
import { AlertComponent } from './_components';
import { ProfileComponent } from './profile/profile.component';
import { AgmCoreModule } from '@agm/core';
import { RegisterChefeComponent } from './register-chefe/register-chefe.component';
import { ListagensUsersComponent } from './listagens-users/listagens-users.component';
import { VerRotaComponent } from './ver-rota/ver-rota.component';
import { CamiaoComponent } from './camioes/camiao.component';
import { CamiaoAddEditComponent } from './camioes/camiao-add-edit.component';
import { FooterComponent } from './footer/footer.component';
import { EcopontoAddEditComponent } from './ecopontos/ecoponto-add-edit.component';
import { EcopontoListComponent } from './ecopontos/ecoponto-list.component';
import { Ecoponto } from './_models/ecoponto';
import { MapsComponent } from './maps/maps.component';
import { NotificationComponent } from './notification/notification.component';
//mapa
/// <reference path="node_modules/bingmaps/types/MicrosoftMaps/Microsoft.Maps.All.d.ts" />
import { Component, VERSION } from '@angular/core'
import {
    MapModule, MapAPILoader, MarkerTypeId, IMapOptions, IBox, IMarkerIconInfo, WindowRef,
    DocumentRef, MapServiceFactory,
    BingMapAPILoaderConfig, BingMapAPILoader,
    GoogleMapAPILoader, GoogleMapAPILoaderConfig
} from 'angular-maps';
import { UpdateUserComponent } from './update-user/update-user.component';
import { ListagemNotificacaoComponent } from './listagem-notificacoes/listagem-notificacoes.component';
import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        appRoutingModule,
        MapModule.forRootBing()
    ],
    declarations: [
        AppComponent,
        HomeComponent,
        AdminComponent,
        LoginComponent,
        RegisterComponent,
        FooterComponent,
        AlertComponent,
        ProfileComponent,
        ListagensUsersComponent,
        EcopontoListComponent,
        EcopontoAddEditComponent,
        RegisterChefeComponent,
        VerRotaComponent,
        CamiaoComponent,
        CamiaoAddEditComponent,
        MapsComponent,
        UpdateUserComponent,
        NotificationComponent,
        ListagemNotificacaoComponent,
        DashboardComponent
    ],
    providers: [
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },
        //{ provide: MapAPILoader, deps: [], useFactory: BingMapServiceProviderFactory }

        // provider used to create fake backend
        fakeBackendProvider
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

/**
export function BingMapServiceProviderFactory() {
    let bc: BingMapAPILoaderConfig = new BingMapAPILoaderConfig();
    bc.apiKey = "AoduSJw7y1GsQMOoUoc4N4z2l0j8OPChsLhLyHM6XX0uFYUPV_xFqv9FKUvjFZUN"; //bing map key
    bc.branch = "experimental";
    return new BingMapAPILoader(bc, new WindowRef(), new DocumentRef());
}
*/