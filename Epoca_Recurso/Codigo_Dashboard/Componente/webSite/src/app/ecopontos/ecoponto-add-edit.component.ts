﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { EcopontoService, PubSubService } from '../_services/index';
// import slide in/out animation
import { slideInOutAnimation } from '../_animations/index';
import { AlertService } from '../_services';

@Component({
    templateUrl: 'ecoponto-add-edit.component.html',

    // make slide in/out animation available to this component
    animations: [slideInOutAnimation],

    // attach the slide in/out animation to the host (root) element of this component
    host: { '[@slideInOutAnimation]': '' }
})
export class EcopontoAddEditComponent implements OnInit {
    title: string;
    ecoponto: any = {};
    submitted = false;
    ecopontoForm: FormGroup;
    loading = false;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private ecopontoService: EcopontoService,
        private pubSubService: PubSubService,
        private alertService: AlertService
    ) { }

    ngOnInit() {
        this.ecopontoForm = this.formBuilder.group({
            sensor: ['', Validators.required],
            distrito: ['', Validators.required],
            concelho: ['', Validators.required],
            freguesia: ['', Validators.required],
            rua: ['', Validators.required],
            codigoPostal: ['', [Validators.required, Validators.maxLength(8), Validators.minLength(8)]],
            lat: ['', Validators.required],
            lon: ['', Validators.required],
            tipo: ['', Validators.required],
            altura: ['', Validators.required],
            alturaLivre: ['', Validators.required],
            capacidade: ['', Validators.required]
        });

        this.title = 'Add Ecoponto';
        const ecopontoId = Number(this.route.snapshot.params['id']);
        if (ecopontoId) {
            this.title = 'Edit Ecoponto';
            this.ecopontoService.getById(ecopontoId).subscribe(x => this.ecoponto = x);
        }
    }

    // convenience getter for easy access to form fields
    get f() { return this.ecopontoForm.controls; }

    saveEcoponto() {
        // save ecoponto
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.ecopontoForm.invalid) {
            return;
        }

        this.loading = true;

        const action = this.ecoponto.id ? 'update' : 'create';
        this.ecopontoService[action](this.ecoponto)
            .subscribe(
                data => {
                    this.alertService.success('Ecoponto successful', true);
                    this.submitted = false;

                    // redirect to ecopontos view
                    this.router.navigate(['ecopontos']);

                    // publish event so list component refreshes
                    this.pubSubService.publish('ecopontos-updated');
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}