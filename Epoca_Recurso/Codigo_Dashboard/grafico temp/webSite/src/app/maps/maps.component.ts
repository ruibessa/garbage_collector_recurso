import { Component, NgModule, VERSION, Input } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {
  MapModule, MapAPILoader, MarkerTypeId, IMapOptions, IBox, IMarkerIconInfo, WindowRef, DocumentRef, MapServiceFactory,
  BingMapAPILoaderConfig, BingMapAPILoader,
  GoogleMapAPILoader, GoogleMapAPILoaderConfig
} from 'angular-maps';
import { environment } from '@environments/environment';

import { EcopontoService, PubSubService } from '../_services/index';
import { Subscription, from } from 'rxjs';



import * as jsgraphs from "js-graph-algorithms";

import { Ecoponto } from './../_models/ecoponto';
import { ConvertActionBindingResult } from '@angular/compiler/src/compiler_util/expression_converter';

@Component({
  selector: 'app-maps',
  templateUrl: './maps.component.html',
  styleUrls: ['./maps.component.less']

})

export class MapsComponent {

  map: any;
  pushpin: any;
  locPushpin: String;
  ecopontos = new Array<Ecoponto>();
  ecopontosOutros = new Array<Ecoponto>();
  ecopontosOrdenadosRota = new Array<Ecoponto>();
  numberEcopontos: number;
  wpecopontos = new Array<Ecoponto>();
  subscription: Subscription;
  numeroEcopontos: number;
  distancias = new Array<number>();
  dist = [];

  constructor(
    private ecopontoService: EcopontoService,
    private pubSubService: PubSubService

  ) { }

  ngOnInit() {
    if (environment.tipoMapa == 0) { // componente home
      this.loadWPEcopontos();


    }

    else if (environment.tipoMapa == 1) { // componente rota
      this.loadEcopontos();




    } else if (environment.tipoMapa == 2) { // componente notificacao
      this.loadMapaNotificacao();
    }

  }


  loadMapaRota() {
    this.map = new Microsoft.Maps.Map(document.getElementById('myMap'), {
      maxZoom: 20,
      minZoom: 5,
      credentials: 'AoduSJw7y1GsQMOoUoc4N4z2l0j8OPChsLhLyHM6XX0uFYUPV_xFqv9FKUvjFZUN'
    });

    this.createRoute();
  }

  loadMapaNotificacao() {
    this.map = new Microsoft.Maps.Map(document.getElementById('myMap'), {
      maxZoom: 20,
      minZoom: 5,
      credentials: 'AoduSJw7y1GsQMOoUoc4N4z2l0j8OPChsLhLyHM6XX0uFYUPV_xFqv9FKUvjFZUN'
    });

    Microsoft.Maps.Events.addHandler(this.map, 'click', (e) => { this.loadPushpin('mapClick', e); });

  }

  loadMapaHome() {
    this.map = new Microsoft.Maps.Map(document.getElementById('myMap'), {
      maxZoom: 20,
      minZoom: 5,
      credentials: 'AoduSJw7y1GsQMOoUoc4N4z2l0j8OPChsLhLyHM6XX0uFYUPV_xFqv9FKUvjFZUN'
    });



  }


  private loadEcopontos() {
    this.ecopontoService.getRota(55).subscribe(response => {
      this.ecopontos = new Array<Ecoponto>();
      this.ecopontosOutros = new Array<Ecoponto>();
      this.ecopontosOrdenadosRota = new Array<Ecoponto>();
      this.dist = [];

      this.numeroEcopontos = response.length;

      var contador = 0;
      for (const index in response) {
        this.ecopontos.push(response[index]);
        this.ecopontosOutros.push(response[index]);
        this.dist[contador] = response[index];
        contador = contador + 1;
      }

      //this.selecionarOrdem();
      this.selecionarOrdemEcopontosComArrayDistancias();
      this.loadMapaRota();
    });
  }

  private loadWPEcopontos() {
    this.ecopontoService.getAll().subscribe(response => {
      this.wpecopontos = new Array<Ecoponto>();


      console.log("qual op lenght" + response.length);
      this.numeroEcopontos = response.length;

      for (const index in response) {
        console.log("inaaadaaaex" + index);
        console.log(index.toString());

        this.wpecopontos.push(response[index]);

        console.log(response[index]);
        console.log("ARRAY --- > " + this.wpecopontos);
      }

      this.createWPEcopontos();

    });
  }

  // cria pushpin para notificações
  loadPushpin(id: string, e: any) {

    // apaga entradas se ouver
    if (this.map.entities.getLength() > 0) {
      for (var i = this.map.entities.getLength() - 1; i >= 0; i--) {
        var pushpin = this.map.entities.get(i);
        if (pushpin instanceof Microsoft.Maps.Pushpin) {
          this.map.entities.removeAt(i);
        }
      }
      console.log("apagou");
    }


    //verifica a localizacao no click e insere pushpin
    if (e.targetType == "map") {

      var point = new Microsoft.Maps.Point(e.getX(), e.getY());
      var loc = e.target.tryPixelToLocation(point);

      if (loc != null) {
        console.log("chamou");
        this.pushpin = new Microsoft.Maps.Pushpin(loc);
        this.pushpin.setOptions({ enableHoverStyle: false, enableClickedStyle: false, draggable: false });

        this.map.entities.push(this.pushpin);
        document.getElementById("printoutPanel").innerHTML = "The location " + loc.latitude + ", " + loc.longitude + " was clicked."
        environment.localidadeLatitude = loc.latitude;
        environment.localidadeLongitude = loc.longitude;
      }

      this.locPushpin = loc.latitude.toString();
      console.log("Latitude " + this.locPushpin);
      this.locPushpin = loc.longitude.toString();
      console.log("Longitude " + this.locPushpin);
    }

  }

  //Selecionar Ordem de Recolha dos Ecopontos
  private selecionarOrdem() {
    var g = new jsgraphs.WeightedDiGraph(this.ecopontos.length * this.ecopontos.length); // number vertices in the graph

    console.log("ECOPONTOS ARRAY " + this.ecopontos.toString());
    let contadorI: number = 0;
    let contadorX: number = 0;
    Microsoft.Maps.loadModule("Microsoft.Maps.SpatialMath", () => {

      for (let i of this.ecopontos) {
        var loc = new Microsoft.Maps.Location(i.lat, i.lon)
        contadorX = 0;
        for (let x of this.ecopontosOutros) {


          console.log("ECO ORDEM --> " + this.ecopontos);
          console.log("ECO origem--> " + i.id);
          console.log("ECO destino --> " + x.id);

          var loc2 = new Microsoft.Maps.Location(x.lat, x.lon)

          var pushpin1 = new Microsoft.Maps.Pushpin(loc);
          pushpin1.setOptions({ enableHoverStyle: false, enableClickedStyle: false, draggable: false });
          var pushpin2 = new Microsoft.Maps.Pushpin(loc2);
          pushpin1.setOptions({ enableHoverStyle: false, enableClickedStyle: false, draggable: false });

          var distance = Microsoft.Maps.SpatialMath.getDistanceTo(pushpin1.getLocation(), pushpin2.getLocation(), Microsoft.Maps.SpatialMath.DistanceUnits.Kilometers);
          console.log("Distancia " + distance);

          g.addEdge(new jsgraphs.Edge(contadorX, contadorI, distance));
          contadorX = contadorX + 1;
        }
        contadorI = contadorI + 1;
      };

    });

    // g.addEdge(new jsgraphs.Edge(0, 0, 99999));
    // //chamar o bingmaps para saber a distancia do ponto 0 ao ponto 1
    // g.addEdge(new jsgraphs.Edge(0, 1, 0.22));

    // g.node(2).label = 'Hello';
    // g.edge(0, 2).label = 'World'; // edge from node 4 to node 5

    console.log(g.V); // display the number of vertices in g
    console.log(g.adj(0)); // display the adjacency list which are directed edges from vertex 0
    console.log(g.adj(1));
    console.log(g.adj(2));


    // var kruskal = new jsgraphs.KruskalMST(g);
    // var mst = kruskal.mst;
    // for (let k of mst) {
    //   var e = k;
    //   var v = e.either();
    //   var w = e.other(v);
    //   console.log("(" + v + ". " + w + ") : " + e.weight);
    // }

    // var bf = new jsgraphs.TopologicalSortShortestPaths(g, 2);

    // for (var v = 1; v < g.V; ++v) {
    //   if (bf.hasPathTo(v)) {
    //     var path = bf.pathTo(v);
    //     console.log('=====path from 0 to ' + v + ' start==========');
    //     for (var i = 0; i < path.length; ++i) {
    //       var e = path[i];
    //       console.log(e.from() + ' => ' + e.to() + ': ' + e.weight);
    //     }
    //     console.log('=====path from 0 to ' + v + ' end==========');
    //     console.log('=====distance: ' + bf.distanceTo(v) + '=========');
    //   }
    // }


    // var bf = new jsgraphs.BellmanFord(g, 0);

    // for (var v = 1; v < g.V; ++v) {
    //   if (bf.hasPathTo(v)) {
    //     var path = bf.pathTo(v);
    //     console.log('=====path from 0 to ' + v + ' start==========');
    //     for (var i = 0; i < path.length; ++i) {
    //       var e = path[i];
    //       console.log(e.from() + ' => ' + e.to() + ': ' + e.weight);
    //     }
    //     console.log('=====path from 0 to ' + v + ' end==========');
    //     console.log('=====distance: ' + bf.distanceTo(v) + '=========');
    //   }
    // }

    // var dijkstra = new jsgraphs.Dijkstra(g, 0);

    // for (var v = 1; v < g.V; ++v) {
    //   if (dijkstra.hasPathTo(v)) {
    //     var path = dijkstra.pathTo(v);
    //     console.log('=====path from 0 to ' + v + ' start==========');
    //     for (var i = 0; i < path.length; ++i) {
    //       var e = path[i];
    //       console.log(e.from() + ' => ' + e.to() + ': ' + e.weight);
    //     }
    //     console.log('=====path from 0 to ' + v + ' end==========');
    //     console.log('=====distance: ' + dijkstra.distanceTo(v) + '=========');
    //   }
    // }
  }

  //Selecionar Ordem de Recolha dos Ecopontos
  private selecionarOrdemEcopontosComArrayDistancias() {
    this.ecopontosOrdenadosRota.push(this.dist[0]); //adicionar a origem a rota

    var numberOfTimesToCalculeDistances = this.ecopontos.length; //numero de vezes que precisa de calcular distancias
    var numberEco = this.ecopontos.length; //numero de ecopontos para recolha

    this.distancias = new Array<number>(); //array de distancias

    Microsoft.Maps.loadModule("Microsoft.Maps.SpatialMath", () => {

      for (let numberOfTimes = 0; numberOfTimes < numberOfTimesToCalculeDistances - 2; numberOfTimes++) {
        this.dist = this.ecopontos.slice(0, this.ecopontos.length); //retorna em formato array o intervalo requisitado
        console.log("VAI AJUDAR BASTANTE O SLICE" + this.dist);
        console.log("VAI AJUDAR BASTANTE O SLICE" + this.dist[0].lat);

        for (let i = 1; i <= numberEco - 1; i++) { //para calcular as distancias de um ecoponto para todos os outros
          var loc = new Microsoft.Maps.Location(this.dist[0].lat, this.dist[0].lon); //ecoponto origem
          var loc2 = new Microsoft.Maps.Location(this.dist[i].lat, this.dist[i].lon); //ecoponto destino

          var pushpin1 = new Microsoft.Maps.Pushpin(loc);
          pushpin1.setOptions({ enableHoverStyle: false, enableClickedStyle: false, draggable: false });
          var pushpin2 = new Microsoft.Maps.Pushpin(loc2);
          pushpin2.setOptions({ enableHoverStyle: false, enableClickedStyle: false, draggable: false });

          this.distancias.push(Microsoft.Maps.SpatialMath.getDistanceTo(pushpin1.getLocation(), pushpin2.getLocation(), Microsoft.Maps.SpatialMath.DistanceUnits.Kilometers)); //adicionar distancia ao array

        };
        var distanciaMinima = Math.min(...this.distancias); //calcular distancia minima
        console.log(this.distancias.toString());
        console.log("DISTANCIA MINIMA: " + distanciaMinima);

        var cont = 1;

        for (let i of this.distancias) {
          if (i == distanciaMinima) {
            console.log("proximo ecoponto adicionado: " + this.dist[cont].freguesia);
            this.ecopontosOrdenadosRota.push(this.dist[cont]); //adicionar o proximo ecoponto a rota

            this.ecopontos.shift(); //apagar a antiga origem

            var ultimoDestinoPrimeiraOrigem = this.ecopontos.splice(cont - 1, 1); //para adicionar a proxima origem no inicio

            this.ecopontos.unshift(...ultimoDestinoPrimeiraOrigem); // para colocar o destino anterior, como sendo a proxima origem

            numberEco = numberEco - 1; //como existe menos um ecoponto, calcula menos uma distancia

          } else {
            cont = cont + 1;
          }
        }
      };
      this.ecopontosOrdenadosRota.push(this.ecopontos.pop()); //para adicionar o ultimo ecoponto que falta para concluir pk nao e preciso calcular distancias para so um ecoponto
    });

  }

  //cria as rotas
  createRoute() {

    Microsoft.Maps.loadModule('Microsoft.Maps.Directions', () => {
      var directionsManager = new Microsoft.Maps.Directions.DirectionsManager(this.map);
      directionsManager.setRequestOptions({ routeMode: Microsoft.Maps.Directions.RouteMode.driving });

      //console.log("antes");
      // var arr: Array<{ lat: number, long: number }> = Array(
      //   { "lat": 41.263403329742744, "long": -8.335778261259446 },
      //   { "lat": 41.26727282938952, "long": -8.37579048444707 },
      //   { "lat": 41.26367482640618, "long": -8.347169239682135 }
      // );

      for (let i of this.ecopontosOrdenadosRota) {
        console.log("ECO ROTA --> " + i);
        directionsManager.addWaypoint(new Microsoft.Maps.Directions.Waypoint({
          address: i.rua,
          location: new Microsoft.Maps.Location(i.lat, i.lon)
        }));

        //console.log("iiiiiii  " + i);

      }

      directionsManager.setRenderOptions({ itineraryContainer: document.getElementById('printoutPanel') });
      directionsManager.calculateDirections();
      //console.log("calcula");

      //var wp1 = new Microsoft.Maps.Directions.Waypoint({
      //  address: '1',
      //  location: new Microsoft.Maps.Location(41.263403329742744, -8.335778261259446)
      //});

      //var wp2 = new Microsoft.Maps.Directions.Waypoint({
      //  address: '2',
      //  location: new Microsoft.Maps.Location(41.26727282938952, -8.37579048444707)
      // });

      //var wp3 = new Microsoft.Maps.Directions.Waypoint({
      //  address: '3',
      //  location: new Microsoft.Maps.Location(41.26367482640618, -8.347169239682135)
      //});

      //var wp4 = new Microsoft.Maps.Directions.Waypoint({
      //  address: '4',
      //  location: new Microsoft.Maps.Location(41.27135106460532, -8.366094084344926)
      //});

      ///directionsManager.addWaypoint(wp2);
      //directionsManager.addWaypoint(wp3);
      //directionsManager.addWaypoint(wp4);

    });

  }


  //create all points in home 
  createWPEcopontos() {

    this.loadMapaHome();
    // var arr2: Array<{ lat: number, long: number, estado: number }> = Array(
    //   { "lat": 41.263403329742744, "long": -8.335778261259446, "estado": 20 },
    //   { "lat": 41.26727282938952, "long": -8.37579048444707, "estado": 55 },
    //   { "lat": 41.26367482640618, "long": -8.347169239682135, "estado": 80 }
    // );

    for (let i of this.wpecopontos) {
      var loc = new Microsoft.Maps.Location(i.lat, i.lon)
      if (i.estado >= 0 && i.estado < 25) {
        this.pushpin = new Microsoft.Maps.Pushpin(loc, { icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="32" viewBox="0 0 30 32" xml:space="preserve"><circle cx="15" cy="15" r="12" style="fill:#00ff00;stroke:{color};stroke-width:3;"/><polygon fill="{color}" points="12,27 18,27 15,32 12,27"/><text x="15" y="18" style="font-size:10px;font-family:arial;fill:#ffffff;" text-anchor="middle">{text}</text></svg>', anchor: new Microsoft.Maps.Point(15, 32), roundClickableArea: true, color: '#000000', text: '' });
      } else if (i.estado >= 25 && i.estado < 50) {
        this.pushpin = new Microsoft.Maps.Pushpin(loc, { icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="32" viewBox="0 0 30 32" xml:space="preserve"><circle cx="15" cy="15" r="12" style="fill:#0000ff;stroke:{color};stroke-width:3;"/><polygon fill="{color}" points="12,27 18,27 15,32 12,27"/><text x="15" y="18" style="font-size:10px;font-family:arial;fill:#ffffff;" text-anchor="middle">{text}</text></svg>', anchor: new Microsoft.Maps.Point(15, 32), roundClickableArea: true, color: '#000000', text: '' });
      } else if (i.estado >= 50 && i.estado < 75) {
        this.pushpin = new Microsoft.Maps.Pushpin(loc, { icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="32" viewBox="0 0 30 32" xml:space="preserve"><circle cx="15" cy="15" r="12" style="fill:#ffff00;stroke:{color};stroke-width:3;"/><polygon fill="{color}" points="12,27 18,27 15,32 12,27"/><text x="15" y="18" style="font-size:10px;font-family:arial;fill:#ffffff;" text-anchor="middle">{text}</text></svg>', anchor: new Microsoft.Maps.Point(15, 32), roundClickableArea: true, color: '#000000', text: '' });
      } else if (i.estado >= 75) {
        this.pushpin = new Microsoft.Maps.Pushpin(loc, { icon: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="32" viewBox="0 0 30 32" xml:space="preserve"><circle cx="15" cy="15" r="12" style="fill:#e00505;stroke:{color};stroke-width:3;"/><polygon fill="{color}" points="12,27 18,27 15,32 12,27"/><text x="15" y="18" style="font-size:10px;font-family:arial;fill:#ffffff;" text-anchor="middle">{text}</text></svg>', anchor: new Microsoft.Maps.Point(15, 32), roundClickableArea: true, color: '#000000', text: '' });
      }

      this.pushpin.setOptions({ enableHoverStyle: false, enableClickedStyle: false, draggable: false });
      this.map.entities.push(this.pushpin);
    }
    console.log("marcadores");
  }





  // ngOnLoad() {
  //   this.loadMapa();
  // }


  // loadMapa() {
  //   this.map = new Microsoft.Maps.Map(
  //     document.getElementById('myMap'),
  //     {
  //       zoom: 50
  //     }
  //   );
  // }

  // function loadMapScenario(): void {
  //   this.map = new Microsoft.Maps.Map(
  //     document.getElementById('myMap'),
  //     {
  //       zoom: 50
  //     }
  //   );
  // }



}

