import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})

export class DashboardComponent implements OnInit {

  Highcharts = Highcharts;
  chartOptions = {
    title: '',
    chart: {
      type: 'column'
  }, 
  series: [{
    data: [{ y: 10,name: 'Numero de ecopontos verdes', color: '#23bf0b'},{ y: 20, name: 'Numero de ecopontos azuis', color: '#0b23bf'},
           { y: 30, name: 'Numero de ecopontos amarelos', color: '#bfa70b'}, { y: 40, name: 'Numero de ecopontos vermelhos', color: '#bf0b23'}]
}]
  };

  constructor() {}

  ngOnInit() {}
  }
