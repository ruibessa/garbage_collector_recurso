import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';

import { AlertService, EmpresaService, UserService, AuthenticationService } from '../_services';

@Component({
  selector: 'app-register-chefe',
  templateUrl: './register-chefe.component.html',
  styleUrls: ['./register-chefe.component.less']
})
export class RegisterChefeComponent implements OnInit {
  registerChefeForm: FormGroup;
  loading = false;
  submitted = false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private authenticationService: AuthenticationService,
    private empresaService: EmpresaService,
    private alertService: AlertService) {
      if (this.authenticationService.currentUserValue) {
        this.router.navigate(['/home']);
    }
  }

  ngOnInit() {
    this.registerChefeForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      username: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      nif: ['', [Validators.required, Validators.maxLength(9), Validators.minLength(9)]],
      nomeEmpresa: ['', Validators.required],
      localEmpresa: ['', Validators.required]

    });
  }
  get f() { return this.registerChefeForm.controls; }

  onSubmit() {
    this.submitted = true;

    // reset alerts on submit
    this.alertService.clear();

    // stop here if form is invalid
    if (this.registerChefeForm.invalid) {
      return;
    }



    this.loading = true;
    this.empresaService.registerEmpresa(this.registerChefeForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Registration successful', true);
          this.router.navigate(['/home']);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });

  }
}


