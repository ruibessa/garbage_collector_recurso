import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CamiaoComponent } from './camiao.component';

describe('CamiaoComponent', () => {
  let component: CamiaoComponent;
  let fixture: ComponentFixture<CamiaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CamiaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CamiaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
