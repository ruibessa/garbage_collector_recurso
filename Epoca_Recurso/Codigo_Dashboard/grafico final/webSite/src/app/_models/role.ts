export enum Role {
    Admin = "Admin",
    Gestor_Chefe = "Gestor_Chefe",
    Gestor = "Gestor",
    Camionista = "Camionista",
    Cidadao = "Cidadao"
}