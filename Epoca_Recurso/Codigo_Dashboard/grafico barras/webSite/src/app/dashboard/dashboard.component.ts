import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})

export class DashboardComponent implements OnInit {

  Highcharts = Highcharts;
  chartOptions = {
    chart: {
      type: 'column'
  },
    series: [{
      data: [1, 2, 3]
    }]
  };

  constructor() {}

  ngOnInit() {}
  }
