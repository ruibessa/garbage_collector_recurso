﻿import { Ecoponto } from './../_models/ecoponto';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { EcopontoService, PubSubService } from '../_services/index';

// import fade in animation
import { fadeInAnimation } from '../_animations/index';
@Component({
    templateUrl: 'ecoponto-list.component.html',

    // make fade in animation available to this component
    animations: [fadeInAnimation],

    // attach the fade in animation to the host (root) element of this component
    host: { '[@fadeInAnimation]': '' }
})
export class EcopontoListComponent implements OnInit, OnDestroy {
    ecopontos: any[];
    subscription: Subscription;
    loading = false;

    constructor(
        private ecopontoService: EcopontoService,
        private pubSubService: PubSubService
    ) { }

    ngOnInit() {
        this.loadEcopontos();
        // reload ecopontos when updated
        this.subscription = this.pubSubService.on('ecopontos-updated').subscribe(() => this.loadEcopontos());
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.subscription.unsubscribe();
    }

    deleteEcoponto(id: number) {
        this.ecopontos.find(x => x.id === id).deleting = true;
        this.ecopontoService.delete(id).subscribe(() => {
            // remove ecoponto from ecopontos array after deleting
            this.ecopontos = this.ecopontos.filter(x => x.id !== id);
        });
    }

    private loadEcopontos() {
        this.ecopontoService.getAll().subscribe(x => this.ecopontos = x);
    }

}