import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environments/environment';
import { Notification, User } from '../_models';


@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private http: HttpClient) { }

getAll() {
    return this.http.get<Notification[]>(`${environment.apiUrl}/Notificacoes`);
}

getById(id: number) {
    return this.http.get<Notification>(`${environment.apiUrl}/Notificacoes/${id}`);
}

register(notification: Notification, userId: number) {
  notification.longitude = environment.localidadeLongitude +"";
  notification.latitude = environment.localidadeLatitude + "";

  notification.imagem = environment.imageUrl +"";
  notification.userId = userId;

  console.log(" Longitude: " + notification.longitude + 
  "Description: " + notification.descricao);
  console.log("ImageURL: " + notification.imagem);  

    return this.http.post(`${environment.apiUrl}/Notificacoes/register`, notification);
}

delete(id: number) {
    return this.http.delete(`${environment.apiUrl}/Notificacoes/${id}`);
}
}
