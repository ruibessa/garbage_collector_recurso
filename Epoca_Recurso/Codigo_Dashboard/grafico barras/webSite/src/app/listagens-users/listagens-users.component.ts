import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { User, Role } from '../_models';
import { UserService, AuthenticationService } from '../_services';

@Component({
  selector: 'app-listagens-users',
  templateUrl: './listagens-users.component.html',
  styleUrls: ['./listagens-users.component.less']
})
export class ListagensUsersComponent implements OnInit {
  currentUser: User;
  users = [];
  loading = false;
  userFromApi: User;
  role: boolean = false;
  roleGestor: boolean = false;
  roleCamionista: boolean = false;


  constructor(private authenticationService: AuthenticationService,
    private userService: UserService
  ) {
    this.currentUser = this.authenticationService.currentUserValue;
    this.userService.getById(this.authenticationService.currentUserValue.id).subscribe(x => {
      this.currentUser.empresa = x.empresa;
      this.currentUser.firstName = x.firstName;
      this.currentUser.lastName = x.lastName;
    });


    if (this.currentUser.role == Role.Admin) {
      this.role = true;
    };
    if (this.currentUser.role == Role.Gestor_Chefe) {
      this.roleGestor = true;
    };
    if (this.currentUser.role == Role.Gestor) {
      this.roleGestor = true;
    };
    if (this.currentUser.role == Role.Camionista) {
      this.roleCamionista = true;
    };
    if (this.currentUser.role == Role.Cidadao) {
      this.roleCamionista = true;
    };
  }

  ngOnInit() {
    this.loading = true;
    this.loadUsersEmpresa();
  }

  deleteUser(id: number) {
    this.userService.delete(id)
      .pipe(first())
      .subscribe(() => this.loadAllUsers());
  }

  private loadAllUsers() {
    this.userService.getAll()
      .pipe(first())
      .subscribe(users => this.users = users);
  }

  private loadUsersEmpresa() {
    this.userService.GetAllEmpresa(this.currentUser.id).subscribe(x => {this.users = x;this.loading = false;});
  }

}
