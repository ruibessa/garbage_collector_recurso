import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterChefeComponent } from './register-chefe.component';

describe('RegisterChefeComponent', () => {
  let component: RegisterChefeComponent;
  let fixture: ComponentFixture<RegisterChefeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterChefeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterChefeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
