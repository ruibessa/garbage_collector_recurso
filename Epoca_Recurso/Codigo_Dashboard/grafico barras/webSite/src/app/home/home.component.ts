﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { environment } from './../../environments/environment';

import { User, Role } from '../_models';
import { UserService, AuthenticationService } from '../_services';


@Component({
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    currentUser: User;
    users = [];
    loading = false;
    userFromApi: User;
    lat: number = 41.276382;
    long: number = -8.375487;

    constructor(
        private authenticationService: AuthenticationService,
        private userService: UserService
    ) { this.currentUser = this.authenticationService.currentUserValue; }

    ngOnInit() {
        this.currentUser = this.authenticationService.currentUserValue;
        if (this.currentUser != null) {
            this.loadAllUsers();
            this.loading = true;
            this.userService.getById(this.currentUser.id).pipe(first()).subscribe(user => {
                this.loading = false;
                this.userFromApi = user;
            });
        }
        environment.tipoMapa = 0
    }

    deleteUser(id: number) {
        this.userService.delete(id)
            .pipe(first())
            .subscribe(() => this.loadAllUsers());
    }

    private loadAllUsers() {
        this.userService.getAll()
            .pipe(first())
            .subscribe(users => this.users = users);
    }


}