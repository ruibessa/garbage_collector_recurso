using System;

namespace WebApi.Entities
{
    public class Evento
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Color { set; get; }
        public string ColorS { set; get; }
        public string Start { set; get; }
        public string End { set; get; }
        public string IdEmpresa { set; get; }
    }
}