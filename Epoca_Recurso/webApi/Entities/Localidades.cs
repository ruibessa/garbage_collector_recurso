namespace WebApi.Entities
{
    public class Localidade
    {
        public int id { get; set; }
        public int parent_id { get; set; }
        public int level { get; set; }
        public string name { get; set; }
    }
}