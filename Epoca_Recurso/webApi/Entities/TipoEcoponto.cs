namespace WebApi.Entities
{
    public static class TipoEcoponto
    {
        public const string Organico = "Organico";
        public const string Plastico = "Plastico";
        public const string Vidro = "Vidro";
        public const string Cartao = "Cartao";
        public const string Monstro = "Monstro";
    }
}