namespace WebApi.Entities
{
    public static class EstadoEcoponto
    {
        public const string Vazio = "Vazio";
        public const string Menos_Metade = "Menos_Metade";
        public const string Mais_Metade = "Mais_Metade";
        public const string Cheio = "Cheio";
    }
}