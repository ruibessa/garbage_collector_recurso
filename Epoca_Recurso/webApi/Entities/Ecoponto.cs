using System;

namespace WebApi.Entities
{
    public class Ecoponto
    {
        public int Id { get; set; }
        public string Sensor { get; set; }
        public string Distrito { get; set; }
        public string Concelho { get; set; }
        public string Freguesia { get; set; }
        public string Rua { get; set; }
        public string CodigoPostal { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }
        public string Tipo { get; set; }
        public double Altura { get; set; }
        public double AlturaLivre { get; set; }
        public double Capacidade { get; set; }
        public double Estado { get; set; }
    }
}