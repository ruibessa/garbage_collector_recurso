using System;

namespace WebApi.Entities
{
    public class Camiao
    {
        public int Id { get; set; }
        public string Matricula { set; get; }
        public int Carga { set; get; }
        public string Tipo { set; get; }
        public string Distrito { set; get; }
        public string Concelho { set; get; }
        public string Empresa {set; get;}
    }
}