using System.ComponentModel.DataAnnotations;

namespace WebApi.Entities
{
    public class Empresa
    {
        [Key]
        public string Nif { get; set; }
        public string NomeEmpresa { get; set; }
        public string LocalizacaoEmpresa { get; set; }
    }
}