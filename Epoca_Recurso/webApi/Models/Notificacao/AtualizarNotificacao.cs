using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Notificacao
{
    public class AtualizarNotificacao
    {
        [Required]
        public int Id { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }
        public string Descricao { get; set; }
        public string Imagem { get; set; }
        public int UserId { get; set; }
        public string Estado { get; set; }



    }
}