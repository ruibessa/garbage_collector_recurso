using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Evento
{
    public class EditEvento
    {

        [Required]
        public int Id { get; set; }

        public string Title { get; set; }

        public string Color { set; get; }

        public string ColorS { set; get; }

        public string Start { set; get; }

        public string End { set; get; }
    }
}