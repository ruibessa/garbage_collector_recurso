using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Evento
{
    public class CriarEvento
    {

        [Required]
        public string Title { get; set; }

        [Required]
        public string Color { set; get; }

        [Required]
        public string ColorS { set; get; }

        [Required]
        public string Start { set; get; }

        [Required]
        public string End { set; get; }

        [Required]
        public string IdEmpresa { set; get; }
    }
}