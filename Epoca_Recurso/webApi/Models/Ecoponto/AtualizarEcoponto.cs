using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Ecoponto
{
    public class AtualizarEcoponto
    {
        [Required]
        public int Id { get; set; }

        [Required]
        public string Sensor { get; set; }

        [Required]
        public string Distrito { get; set; }

        [Required]
        public string Concelho { get; set; }

        [Required]
        public string Freguesia { get; set; }

        [Required]
        public string Rua { get; set; }

        [Required]
        public string CodigoPostal { get; set; }

        [Required]
        public double lat { get; set; }

        [Required]
        public double lon { get; set; }

        [Required]
        public string Tipo { get; set; }

        [Required]
        public double Altura { get; set; }

        [Required]
        public double AlturaLivre { get; set; }

        [Required]
        public double Capacidade { get; set; }

        [Required]
        public double Estado { get; set; }
    }
}