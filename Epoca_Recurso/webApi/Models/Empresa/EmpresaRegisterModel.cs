using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Empresa
{
    public class EmpresaRegisterModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }

        public string Role { get; set; }

        [Required]
        public string NomeEmpresa {get;set;}

        public string LocalizacaoEmpresa { get; set; }

        [Required]
        public string Nif { get; set; }
    }
}