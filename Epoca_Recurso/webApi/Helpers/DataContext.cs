using Microsoft.EntityFrameworkCore;
using WebApi.Entities;

namespace WebApi.Helpers
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        public DbSet<User> Users { get; set; }
        public DbSet<Empresa> Empresas { get; set; }
        public DbSet<Ecoponto> Ecopontos { get; set; }
        public DbSet<Notificacao> Notificacoes { get; set; }
        public DbSet<Camiao> Camioes { get; set; }
        public DbSet<Localidade> Localidades {get; set;}
        public DbSet<Evento> Eventos { get; set; }
    }
}