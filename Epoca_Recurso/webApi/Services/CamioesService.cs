using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface ICamioesService
    {
        int GetTotalCamioes();
        Camiao Create(Camiao camiao);
        IEnumerable<Camiao> GetAllCamioes(int userId);
        void Update(Camiao camiaoMudar);
        Camiao GetCamiao(int id);
        void Delete(int id);
    }

    public class CamioesService : ICamioesService
    {
        private DataContext _context;
        private readonly AppSettings _appSettings;

        public CamioesService(DataContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        public Camiao Create(Camiao camiao)
        {
            String Empresa = "";
            try
            {
                Console.WriteLine("\nMatricula: " + camiao.Matricula + "\nTipo: " + camiao.Tipo + "\nCarga: " + camiao.Carga + "\nDistrito: " + camiao.Distrito + "\nConcelho: " + camiao.Concelho + "\nEmpresa " + camiao.Empresa + "\n");
                int result = Int32.Parse(camiao.Empresa);
                User user = _context.Users.Find(result);
                Console.WriteLine("\n" + user.Empresa);
                camiao.Empresa = user.Empresa;
            }
            catch
            {
                throw new AppException("Empresa não encontrada");
            }

            if (string.IsNullOrWhiteSpace(camiao.Matricula))
                throw new AppException("Introduza a Matricula");

            if (_context.Camioes.Any(x => x.Matricula == camiao.Matricula))
                throw new AppException("Matricula já registada no sistema");

            if (camiao.Carga.Equals(null) || camiao.Carga <= 0)
                throw new AppException("Necessário defenir carga Maxima");

            if (string.IsNullOrWhiteSpace(camiao.Tipo))
                throw new AppException("Escolha o tipo de camião este se regista");

            if (string.IsNullOrWhiteSpace(camiao.Distrito) || string.IsNullOrWhiteSpace(camiao.Concelho))
                throw new AppException("Escolha o local onde este camião ira operar normalmente");

            if (string.IsNullOrWhiteSpace(camiao.Empresa) || camiao.Empresa.Length != 9)
                throw new AppException("Empresa não esta associada a este camião");


            if (!_context.Empresas.Any(x => x.Nif == camiao.Empresa))
            {
                Console.WriteLine("Empresa não existe: " + camiao.Empresa);
                throw new AppException("Empresa não existe");
            }

            _context.Camioes.Add(camiao);
            _context.SaveChanges();
            return camiao;

        }

        public void Update(Camiao camiaoNovo)
        {
            var camiaoVelho = _context.Camioes.Find(camiaoNovo.Id);

            if (camiaoVelho == null)
                throw new AppException("Camiao não encontrado");

            if (!string.IsNullOrWhiteSpace(camiaoNovo.Matricula) && camiaoNovo.Matricula != camiaoVelho.Matricula)
            {
                if (_context.Camioes.Any(x => x.Matricula == camiaoNovo.Matricula))
                    throw new AppException("Matricula " + camiaoNovo.Matricula + " já existe");
                camiaoVelho.Matricula = camiaoNovo.Matricula;
            }


            if (!(camiaoNovo.Carga.Equals(null) || camiaoNovo.Carga <= 0))
                camiaoVelho.Carga = camiaoNovo.Carga;

            if (!string.IsNullOrWhiteSpace(camiaoNovo.Tipo))
                camiaoVelho.Tipo = camiaoNovo.Tipo;

            if (!string.IsNullOrWhiteSpace(camiaoNovo.Concelho))
                camiaoVelho.Concelho = camiaoNovo.Concelho;

            if (!string.IsNullOrWhiteSpace(camiaoNovo.Distrito))
                camiaoVelho.Distrito = camiaoNovo.Distrito;

            _context.Camioes.Update(camiaoVelho);
            _context.SaveChanges();
        }

        public IEnumerable<Camiao> GetAllCamioes(int userId)
        {
            Console.WriteLine(userId);
            User user = _context.Users.Find(userId);
            if (user == null)
                throw new AppException("User " + userId + " não existe");

            return _context.Camioes.FromSqlRaw($"SELECT * FROM Camioes Where Empresa='{user.Empresa}'").ToList();
        }


         public int GetTotalCamioes() {
              return _context.Camioes.FromSqlRaw($"SELECT * FROM Camioes Where Empresa>'{0}'").Count();
          
        
        }

        public Camiao GetCamiao(int id)
        {
            return _context.Camioes.Find(id);
        }

        public void Delete(int id)
        {
            var camiao = _context.Camioes.Find(id);
            if (camiao != null)
            {
                _context.Camioes.Remove(camiao);
                _context.SaveChanges();
            }
        }
    }
}