using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using WebApi.Entities;
using WebApi.Helpers;

namespace WebApi.Services
{
    public interface IEventosService
    {
        Evento Create(Evento e);
        void Update(Evento Update);
        IEnumerable<Evento> GetEventos(int userId);
        void Delete (int id);
    }

    public class EventoService : IEventosService
    {
        private DataContext _context;
        private readonly AppSettings _appSettings;

        public EventoService(DataContext context, IOptions<AppSettings> appSettings)
        {
            _context = context;
            _appSettings = appSettings.Value;
        }

        public Evento Create(Evento evento)
        {
            try
            {
                //Console.WriteLine("\nMatricula: " + camiao.Matricula + "\nTipo: " + camiao.Tipo + "\nCarga: " + camiao.Carga + "\nDistrito: " + camiao.Distrito + "\nConcelho: " + camiao.Concelho + "\nEmpresa " + camiao.Empresa + "\n");
                int result = Int32.Parse(evento.IdEmpresa);
                User user = _context.Users.Find(result);
                //Console.WriteLine("\n" + user.Empresa);
                evento.IdEmpresa = user.Empresa;
            }
            catch
            {
                throw new AppException("Empresa não encontrada");
            }
            if (string.IsNullOrWhiteSpace(evento.Title))
            {
                throw new AppException("Introduza um Titulo");
            }
            if (string.IsNullOrWhiteSpace(evento.Color) || string.IsNullOrWhiteSpace(evento.ColorS))
            {
                throw new AppException("Core não selecionada");
            }
            if (string.IsNullOrWhiteSpace(evento.Start))
            {
                throw new AppException("Data inicial não defenida");
            }
            if (string.IsNullOrWhiteSpace(evento.End))
            {
                throw new AppException("Data final não defenida");
            }
            if (string.IsNullOrWhiteSpace(evento.IdEmpresa))
            {
                throw new AppException("Empresa não defenida");
            }

            if (!_context.Empresas.Any(x => x.Nif == evento.IdEmpresa))
            {
                Console.WriteLine("Empresa não existe: " + evento.IdEmpresa);
                throw new AppException("Empresa não existe");
            }

            _context.Eventos.Add(evento);
            _context.SaveChanges();
            return evento;
        }

        public void Update(Evento Update)
        {
            var Old = _context.Eventos.Find(Update.Id);

            if (Old == null)
            {
                throw new AppException("Evento não encontrado");
            }

            if (!string.IsNullOrWhiteSpace(Update.Title) && Update.Title != Old.Title)
            {
                Old.Title = Update.Title;
            }
            if (!string.IsNullOrWhiteSpace(Update.Start) && Update.Start != Old.Start)
            {
                Old.Start = Update.Start;
            }
            if (!string.IsNullOrWhiteSpace(Update.End) && Update.End != Old.End)
            {
                Old.End = Update.End;
            }
            if (!string.IsNullOrWhiteSpace(Update.Color) && Update.Color != Old.Color)
            {
                Old.Color = Update.Color;
            }
            if (!string.IsNullOrWhiteSpace(Update.ColorS) && Update.ColorS != Old.ColorS)
            {
                Old.ColorS = Update.ColorS;
            }

            _context.Eventos.Update(Old);
            _context.SaveChanges();
        }

        public IEnumerable<Evento> GetEventos(int userId)
        {
            User user = _context.Users.Find(userId);
            if (user == null)
                throw new AppException("User " + userId + " não existe");
            return _context.Eventos.FromSqlRaw($"SELECT * FROM Eventos Where IdEmpresa='{user.Empresa}'").ToList();
        }

        public void Delete(int id)
        {
            var evento = _context.Eventos.Find(id);
            if(evento != null)
            {
                _context.Eventos.Remove(evento);
                _context.SaveChanges();
            }
        }
    }
}