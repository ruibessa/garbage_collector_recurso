﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
using WebApi.Helpers;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using WebApi.Services;
using WebApi.Entities;
using WebApi.Models.Notificacao;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class NotificacoesController : ControllerBase
    {
        private INotificacaoService _notificacaoService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public NotificacoesController(
            INotificacaoService userService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _notificacaoService = userService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult Register([FromBody]CriarNotificacao model)
        {

            try
            {
                Console.WriteLine(model.Imagem + " a imagem deveria estar aqui");
                var notificacao = _mapper.Map<Notificacao>(model);
                _notificacaoService.Register(notificacao);
                return Ok(notificacao);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        //[Authorize(Roles = Role.Admin)]
        [AllowAnonymous]
        [HttpGet]
        public IActionResult GetAll()
        {
            var notificacoes = _notificacaoService.GetAll();
            var model = _mapper.Map<IList<NotificacaoModel>>(notificacoes);
            return Ok(model);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            var notificacao = _notificacaoService.GetById(id);
            return Ok(notificacao);
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult Update(int id, [FromBody]AtualizarNotificacao model)
        {
            // map model to entity and set id
            var notificacao = _mapper.Map<Notificacao>(model);
            notificacao.Id = id;

            try
            {
                // update user 
                _notificacaoService.Update(notificacao);
                return Ok(notificacao);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _notificacaoService.Delete(id);
            return Ok();
        }

        [AllowAnonymous]
        [HttpGet("notTotal")]
        public int GetNumeroNotTotal()
        {
            int notTotal = _notificacaoService.GetNumeroNotTotal();
            return notTotal;
        } 

        [AllowAnonymous]
        [HttpGet("notPendentes")]
        public int GetNumeroNotPendentes()
        {
            int notPendentes = _notificacaoService.GetNumeroNotPendentes();
            return notPendentes;
        }

        [AllowAnonymous]
        [HttpGet("notConcluidas")]
        public int GetNumeroNotConcluidas()
        {
            int notConcluidas = _notificacaoService.GetNumeroNotConcluidas();
            return notConcluidas;
        } 

    }
}
