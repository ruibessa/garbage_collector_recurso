using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Evento;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class EventosController : ControllerBase
    {
        private IEventosService _eventoService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public EventosController(
            IEventosService eventoService,
            IMapper mapper,
            IOptions<AppSettings> appSettings
        ){
            _eventoService = eventoService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult RegisterEvento([FromBody]CriarEvento model)
        {
            var evento = _mapper.Map<Evento>(model);
            try{
                _eventoService.Create(evento);
                return Ok(evento);
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult AtualizarEvento(int id, [FromBody]EditEvento model)
        {
            var evento = _mapper.Map<Evento>(model);
            evento.Id = id;
            try{
                _eventoService.Update(evento);
                return Ok(evento);
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpGet("list/{id}")]
        public IActionResult GetEventos(int id)
        {
            var camioes = _eventoService.GetEventos(id);
            return Ok(camioes);
        }

        [AllowAnonymous]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _eventoService.Delete(id);
            return Ok();
        }
    }
}