using System;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Empresa;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class CamioesController : ControllerBase
    {
        private ICamioesService _camiaoService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public CamioesController(
            ICamioesService camiaoService,
            IMapper mapper,
            IOptions<AppSettings> appSettings
        ){
            _camiaoService = camiaoService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult RegisterCamiao([FromBody]CamiaoRegisterModel model)
        {

            var camiao = _mapper.Map<Camiao>(model);
            
            try{
                _camiaoService.Create(camiao);
                return Ok(camiao);
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpPut("{id}")]
        public IActionResult ActualizarCamiao(int id, [FromBody]CamiaoRegisterModel model)
        {
            var camiao = _mapper.Map<Camiao>(model);
            camiao.Id = id;
            try{
                _camiaoService.Update(camiao);
                return Ok(camiao);
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [AllowAnonymous]
        [HttpGet("list/{id}")]
        public IActionResult GetCamioes(int id)
        {
            var camioes = _camiaoService.GetAllCamioes(id);
            return Ok(camioes);
        }

        
        [AllowAnonymous]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            _camiaoService.Delete(id);
            return Ok();
        }

        [AllowAnonymous]
        [HttpGet("totalCamioes")]
        public int GetNumeroTotalCamioes()
        {
            int numberTotalCamiao = _camiaoService.GetTotalCamioes();
            return numberTotalCamiao;
        } 

        [AllowAnonymous]
        [HttpGet("{id}")]
        public IActionResult GetById(int id)
        {
            // only allow admins to access other user records
            //var currentUserId = int.Parse(User.Identity.Name);

            //if (user == null)
            //    return NotFound();

            //if (id != currentUserId && (!User.IsInRole(Role.Admin) || !User.IsInRole(Role.Gestor_Chefe) || !User.IsInRole(Role.Gestor)))
            //    return Forbid();

            var camiao = _camiaoService.GetCamiao(id);

            //SE FOR NECESSARIO, MAS DEVE SER NECESSARIO TROCAR "USER" POR "MODEL"
            //if (user == null)
            //    return NotFound();

            return Ok(camiao);
        }
    }
}