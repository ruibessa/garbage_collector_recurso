using System;
using System.Collections.Generic;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Empresa;
using WebApi.Models.Users;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class EmpresaController : ControllerBase
    {
        private IEmpresaService _empresaService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public EmpresaController(
            IEmpresaService empresaService,
            IMapper mapper,
            IOptions<AppSettings> appSettings
        ){
            _empresaService = empresaService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult RegisterEmpresa([FromBody]EmpresaRegisterModel model)
        {
            // map model to entity
            var user = _mapper.Map<User>(model);
            var empresa = _mapper.Map<Empresa>(model);
            try
            {
                user.Role = "Gestor_Chefe";
                // create user
                _empresaService.CreateCompany(user, model.Password, empresa);
                return Ok(user);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        
    }
}