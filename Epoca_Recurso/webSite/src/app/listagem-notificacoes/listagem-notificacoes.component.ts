import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

// import fade in animation
import { fadeInAnimation } from '../_animations/index';
import { PubSubService, NotificationService } from '@app/_services';
import { User, Role, Notification } from '@app/_models';

@Component({

  templateUrl: './listagem-notificacoes.component.html',
  styleUrls: ['./listagem-notificacoes.component.less']
  // make fade in animation available to this component
})
export class ListagemNotificacaoComponent implements OnInit {
  notificacoes: any[];
  subscription: Subscription;
  loading = false;
  id: number;

  constructor(
    private notificacaoService: NotificationService,
    private pubSubService: PubSubService,
  ) { 

  }

  ngOnInit() {
    this.loadNotificacoes();

    // reload notificactions when updated
    this.subscription = this.pubSubService.on('notificacoes-updated').subscribe(() => this.loadNotificacoes());
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

  deleteNotificacao(id: number) {
    this.notificacoes.find(x => x.id === id).deleting = true;
    this.notificacaoService.delete(id).subscribe(() => {
      // remove notificacao from notificacoes array after deleting
      this.notificacoes = this.notificacoes.filter(x => x.id !== id);
    });
  }

  private loadNotificacoes() {
    this.notificacaoService.getAll().subscribe(x => this.notificacoes = x);
  }
  
isAdmin(){
  
  let utilizador = JSON.parse(localStorage.getItem("currentUser"));
  


  if(utilizador.role == "Admin"){
    return true;
  } else {
    return false;
  }
}

isPendente(notif: Notification){
  if(notif.estado == "pendente"){
    return true;
  } else {
    return false;
  }
}

trocarEstado(notif: Notification){
  this.notificacaoService.updateEstado(notif).subscribe();
}

}
