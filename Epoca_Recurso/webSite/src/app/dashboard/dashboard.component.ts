import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { Ecoponto } from './../_models/ecoponto';
import { Subscription } from 'rxjs';
import { EcopontoService, PubSubService } from '../_services/index';
import { fadeInAnimation, slideInOutAnimation } from '../_animations/index';
import { i18nMetaToDocStmt } from '@angular/compiler/src/render3/view/i18n/meta';
import { NONE_TYPE } from '@angular/compiler';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})

export class DashboardComponent implements OnInit {
  ecopontos: any[];
  Highcharts = Highcharts; 
  chartConstructor = 'chart';
  chartCallback = function (chart) {null}
  updateFlag = false;
  oneToOneFlag = true; 
  runOutsideAngular = false;
  subscription: Subscription;
  loading = false;
  valorTotal = 0;
  ntotalCamiao = 0;
  nNotTotal = 0;
  nNotPendentes = 0;
  nNotConcluidas = 0;
  nUserTotal = 0;
 

  constructor(
    private ecopontoService: EcopontoService,
    private pubSubService: PubSubService,
  ) {
  }

  ngOnInit(): void { 
    if(localStorage.getItem("numberUserTotal")!=undefined && localStorage.getItem("numberUserTotal")!=null) {
      this.nUserTotal = parseInt(localStorage.getItem("numberUserTotal"));
    } else {
    this.nUserTotal = 0;
    }
    if(localStorage.getItem("numberNotConcluidas")!=undefined && localStorage.getItem("numberNotConcluidas")!=null) {
      this.nNotConcluidas = parseInt(localStorage.getItem("numberNotConcluidas"));
    } else {
    this.nNotConcluidas = 0;
    }
    if(localStorage.getItem("numberNotPendentes")!=undefined && localStorage.getItem("numberNotPendentes")!=null) {
      this.nNotPendentes = parseInt(localStorage.getItem("numberNotPendentes"));
    } else {
    this.nNotPendentes = 0;
    }
    if(localStorage.getItem("numberNotTotal")!=undefined && localStorage.getItem("numberNotTotal")!=null) {
      this.nNotTotal = parseInt(localStorage.getItem("numberNotTotal"));
    } else {
    this.nNotTotal = 0;
    }
    if(localStorage.getItem("numbertotalCamioes")!=undefined &&localStorage.getItem("numbertotalCamioes")!=null) {
      this.ntotalCamiao = parseInt(localStorage.getItem("numbertotalCamioes"));
    } else {
    this.ntotalCamiao = 0;
    }
    console.log(localStorage.getItem("numberVerde"));
    console.log(localStorage.getItem("numberAzul"));
    console.log(localStorage.getItem("numberAmarelo"));
    console.log(localStorage.getItem("numberVermelho"));
    let valor1 = parseInt(localStorage.getItem("numberVerde")) + parseInt(localStorage.getItem("numberAzul"));
    let valor2 = parseInt(localStorage.getItem("numberAmarelo"))+parseInt(localStorage.getItem("numberVermelho"));
  

    if(localStorage.getItem("numberVerde")!=undefined &&localStorage.getItem("numberVerde")!=null &&
    localStorage.getItem("numberAzul")!=undefined &&localStorage.getItem("numberAzul")!=null &&
    localStorage.getItem("numberAmarelo")!=undefined &&localStorage.getItem("numberAmarelo")!=null &&
    localStorage.getItem("numberVermelho")!=undefined &&localStorage.getItem("numberVermelho")!=null){
      this.valorTotal = valor1 + valor2;
    } else {
      this.valorTotal = 0;
    }
  }

   chartOptions = { 
    title: "",
    chart: {
      type: 'column'
  },     
  xAxis: {
    title:{
      text: 'Taxa de ocupação'
    },
    categories: ['25%', '50%', '75%','100%']
},
yAxis: {
  title: {
    text: 'Quantidade (uni.)'
  }
},
  series: [{
    showInLegend: false,
    data: [{ y:  parseInt(localStorage.getItem("numberVerde")) ,name: 'Numero de ecopontos no estado verde', color: '#23bf0b'},{ y: parseInt(localStorage.getItem("numberAzul")), name: 'Numero de ecopontos no estado azuil', color: '#0b23bf'},
           { y: parseInt(localStorage.getItem("numberAmarelo")), name: 'Numero de ecopontos no estado amarelo', color: '#bfa70b'}, { y: parseInt(localStorage.getItem("numberVermelho")), name: 'Numero de ecopontos no estado vermelho', color: '#bf0b23'}]
}]

  };
}
