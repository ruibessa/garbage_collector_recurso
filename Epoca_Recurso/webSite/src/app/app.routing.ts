﻿import { UpdateUserComponent } from './update-user/update-user.component';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home';
import { AdminComponent } from './admin';
import { RegisterComponent } from './register';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './_helpers';
import { AuthGuardNotificationService } from './guards/auth-guard-notification.service';
import { Role } from './_models';
import { ProfileComponent } from './profile/profile.component';
import { RegisterChefeComponent } from './register-chefe/register-chefe.component';
import { ListagensUsersComponent } from './listagens-users/listagens-users.component';
import { VerRotaComponent } from './ver-rota/ver-rota.component';
import { EcopontoListComponent, EcopontoAddEditComponent } from './ecopontos';
import { CamiaoComponent } from './camioes/camiao.component';
import { CamiaoAddEditComponent } from './camioes/camiao-add-edit.component';
import { NotificationComponent } from './notification/notification.component';
import { from } from 'rxjs';
import { ListagemNotificacaoComponent } from './listagem-notificacoes/listagem-notificacoes.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CalendarioComponent } from './calendario/calendario.component';

const routes: Routes = [
    //{ path: '', component: HomeComponent, canActivate: [AuthGuard] },
    { path: '', component: HomeComponent },
    {
        path: 'admin',
        component: AdminComponent,
        canActivate: [AuthGuard],
        data: { roles: [Role.Admin] }
    },
    { path: 'login', component: LoginComponent },
    { path: 'updateUser', component: UpdateUserComponent },
    { path: 'register', component: RegisterComponent },
    { path: 'register-chefe', component: RegisterChefeComponent },
    { path: 'profile', component: ProfileComponent },
    { path: 'listagens-users', component: ListagensUsersComponent },
    { path: 'listagem-notificacoes', component: ListagemNotificacaoComponent, canActivate: [AuthGuardNotificationService] },
    { path: 'dashboard', component: DashboardComponent},
    {
        path: 'ecopontos',
        component: EcopontoListComponent,
        children: [
            { path: 'adicionarEcoponto', component: EcopontoAddEditComponent },
            { path: 'editarEcoponto/:id', component: EcopontoAddEditComponent }
        ]
    },
    { path: 'calendario', component: CalendarioComponent },
    { path: 'ver-rota', component: VerRotaComponent },
    { path: 'notification', component: NotificationComponent },

    {
        path: 'camioes',
        component: CamiaoComponent,
        children: [
            { path: 'adicionarCamiao', component: CamiaoAddEditComponent },
            { path: 'editarCamiao/:id', component: CamiaoAddEditComponent }
        ]
    },
    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

export const appRoutingModule = RouterModule.forRoot(routes);

// // for easy import into app module
// export const routedComponents = [
//     EcopontoListComponent,
//     EcopontoAddEditComponent
// ];