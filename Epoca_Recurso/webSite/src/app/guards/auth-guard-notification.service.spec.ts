import { TestBed } from '@angular/core/testing';

import { AuthGuardNotificationService } from './auth-guard-notification.service';

describe('AuthGuardNotificationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AuthGuardNotificationService = TestBed.get(AuthGuardNotificationService);
    expect(service).toBeTruthy();
  });
});
