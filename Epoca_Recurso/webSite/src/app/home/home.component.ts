﻿﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';

import { environment } from './../../environments/environment';

import { User, Role } from '../_models';
import { UserService, AuthenticationService, NotificationService } from '../_services';

import { ToastrService } from 'ngx-toastr';
import { EcopontoService, PubSubService } from '../_services/index';
import {CamiaoService } from '@app/_services';

@Component({
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
    currentUser: User;
    users = [];
    loading = false;
    userFromApi: User;
    lat: number = 41.276382;
    long: number = -8.375487;

    constructor(
        private camiaoService: CamiaoService,
        private ecopontoService: EcopontoService,
        private pubSubService: PubSubService,
        private authenticationService: AuthenticationService,
        private userService: UserService,
        private notificacaoToastr: ToastrService, 
        private notificacaoService: NotificationService
    ) { this.currentUser = this.authenticationService.currentUserValue; }

    ngOnInit() {
     this.numberVerde();
     this.numberAzul();
     this.numberAmarelo();
     this.numberVermelho();
     this.totalCamioes();
     this.numberUserTotal();
     this.numberNotConcluidas();
     this.numberNotPendentes();
     this.numberNotTotal();
     this.currentUser =
     this.authenticationService.currentUserValue;
        if (this.currentUser != null) {
            this.loadAllUsers();
            this.loading = true;
            this.userService.getById(this.currentUser.id).pipe(first()).subscribe(user => {
                this.notificacaoService.getAll().subscribe(data => { 
                    if(user.role !== "Cidadao"){
                    var contador = 0;
                    data.forEach(element => {
                        if(element.estado == "pendente"){
                            contador = contador + 1;
                        }
                    });
                    this.notificacaoShow(contador);
                }
                 });
                this.loading = false;
                this.userFromApi = user;
            });
        }
        environment.tipoMapa = 0
    }

    deleteUser(id: number) {
        this.userService.delete(id)
            .pipe(first())
            .subscribe(() => this.loadAllUsers());
    }

    private loadAllUsers() {
        this.userService.getAll()
            .pipe(first())
            .subscribe(users => this.users = users);
    }

    notificacaoShow(numNotificacoes: number){
        if(numNotificacoes > 0){
            this.notificacaoToastr.info('' + numNotificacoes + ' recolha(s) pendentes', '', { timeOut: 5000, progressBar: true, progressAnimation: "increasing"});
        }
    }

    private numberVerde() {
        this.ecopontoService.getNumberEcopontosVerde().subscribe(x => {
        localStorage.setItem("numberVerde", x.toString());
       });
      }

      
    private numberAzul() {
        this.ecopontoService.getNumberEcopontosAzul().subscribe(x => {
        localStorage.setItem("numberAzul", x.toString());
       });
      }
      
    private numberAmarelo() {
        this.ecopontoService.getNumberEcopontosAmarelo().subscribe(x => {
        localStorage.setItem("numberAmarelo", x.toString());
       });
      }
      
    private numberVermelho() {
        this.ecopontoService.getNumberEcopontosVermelho().subscribe(x => {
        localStorage.setItem("numberVermelho", x.toString());
       });
      }
      private totalCamioes() {
          this.camiaoService.getNumeroTotalCamioes().subscribe(x => {
            localStorage.setItem("numbertotalCamioes", x.toString());
           });
      }

      private numberNotTotal() {
        this.notificacaoService.getNumeroNotTotal().subscribe(x => {
          localStorage.setItem("numberNotTotal", x.toString());
         });
    }
    private numberNotPendentes() {
        this.notificacaoService.getNumeroNotPendentes().subscribe(x => {
          localStorage.setItem("numberNotPendentes", x.toString());
         });
    }
    private numberNotConcluidas() {
        this.notificacaoService.getNumeroNotConcluidas().subscribe(x => {
          localStorage.setItem("numberNotConcluidas", x.toString());
         });
    }
    private numberUserTotal() {
        this.userService.getNumeroUserTotal().subscribe(x => {
          localStorage.setItem("numberUserTotal", x.toString());
         });
    }
}
