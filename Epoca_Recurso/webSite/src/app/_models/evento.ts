export class Evento{
    id: number;
    title: string;
    color: string;
    colorS: string;
    start: Date;
    end: Date;
    idEmpresa: string;
}