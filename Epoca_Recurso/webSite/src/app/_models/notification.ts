﻿export class Notification {
    id: number;
    userId: number;
    descricao: string;
    imagem: string;
    latitude: string;
    longitude: string;
    estado: string;
}