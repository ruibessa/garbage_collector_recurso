export enum EstadoEcoponto {
    Vazio = "Vazio",
    Menos_Metade = "Menos_Metade",
    Mais_Metade = "Mais_Metade",
    Cheio = "Cheio"
}