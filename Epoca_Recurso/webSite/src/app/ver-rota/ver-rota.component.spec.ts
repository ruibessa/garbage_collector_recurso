import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerRotaComponent } from './ver-rota.component';

describe('VerRotaComponent', () => {
  let component: VerRotaComponent;
  let fixture: ComponentFixture<VerRotaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerRotaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerRotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
