import { Ecoponto } from './../_models/ecoponto';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { EcopontoService, PubSubService } from '../_services/index';
import { environment } from '@environments/environment';

@Component({
  selector: 'app-ver-rota',
  templateUrl: './ver-rota.component.html',
  styleUrls: ['./ver-rota.component.less']
})
export class VerRotaComponent implements OnInit {
  ecopontos = new Array<Ecoponto>();
  subscription: Subscription;
  loading = false;

  constructor(
    private ecopontoService: EcopontoService,
    private pubSubService: PubSubService
  ) { }

  ngOnInit() {
    this.loadEcopontos();

    // reload ecopontos when updated
    this.subscription = this.pubSubService.on('ecopontos-updated').subscribe(() => this.loadEcopontos());

    environment.tipoMapa = 1;
  }

  ngOnDestroy() {
    // unsubscribe to ensure no memory leaks
    this.subscription.unsubscribe();
  }

  private loadEcopontos() {
    this.ecopontoService.getRota(1).subscribe(response => {
      this.ecopontos = new Array<Ecoponto>();

      for (const index in response) {
        //  console.log("inaaadaaaex" + index);
        //    console.log(index.toString());

        this.ecopontos.push(response[index]);
        //console.log(response[index]);
      }
    });

  }

}
