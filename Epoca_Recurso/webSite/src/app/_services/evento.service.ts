import { HttpClient } from '@angular/common/http';

import { Injectable } from '@angular/core';
import { environment } from '@environments/environment';
import { Evento } from '@app/_models/evento';

@Injectable({ providedIn: 'root' })
export class EventoService {
    
    constructor(private http: HttpClient) { }

    getAll(id: number) {
        return this.http.get<Evento[]>(`${environment.apiUrl}/Eventos/list/${id}`);
    }

    create(evento: Evento, userId: string) {
        evento.idEmpresa = userId;
        return this.http.post(`${environment.apiUrl}/Eventos/register`, evento);
    }

    update(evento: Evento, userId: string) {
        console.log(evento.id);
        console.log(evento.color);
        console.log(evento.colorS);
        console.log(evento.title);
        console.log(evento.start);
        console.log(evento.end);
        console.log(userId);


        return this.http.put(`${environment.apiUrl}/Eventos/${evento.id}`, evento);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/Eventos/${id}`);
    }
}