﻿import { AuthenticationService } from './authentication.service';
import { environment } from './../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { User, Role } from '../_models';

@Injectable({ providedIn: 'root' })
export class UserService {
    constructor(private http: HttpClient, private authenticationService: AuthenticationService) { }

    getAll() {
        return this.http.get<User[]>(`${environment.apiUrl}/users`);
    }

    getById(id: number) {
        return this.http.get<User>(`${environment.apiUrl}/users/${id}`);
    }

    GetAllEmpresa(id: number) {
        return this.http.get<User[]>(`${environment.apiUrl}/users/empregados/${id}`);
    }

    register(user: User) {
        console.log("APAGAR" + user.role);
        return this.http.post(`${environment.apiUrl}/users/register`, user);
    }

    update(user: User) {
        var id = this.authenticationService.currentUserValue.id;
        user.empresa = this.authenticationService.currentUserValue.empresa;
        console.log(user);
        return this.http.put(`${environment.apiUrl}/users/${id}`, user);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/users/${id}`);
    }
    
    getNumeroUserTotal() {
        return this.http.get<number>(`${environment.apiUrl}/users/userTotal`);
      } 
}