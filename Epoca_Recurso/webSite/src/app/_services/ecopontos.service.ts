﻿import { environment } from '../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Ecoponto } from '../_models';

@Injectable({ providedIn: 'root' })
export class EcopontoService {
    constructor(private http: HttpClient) { }

    getRota(id: number) {
        return this.http.get<Ecoponto[]>(`${environment.apiUrl}/ecopontos/rota/${id}`);
    }

    getAll() {
        return this.http.get<Ecoponto[]>(`${environment.apiUrl}/ecopontos`);
    }

    getNumberEcopontosVerde() {
        return this.http.get<number>(`${environment.apiUrl}/ecopontos/numberverde`);
    } 
    getNumberEcopontosAzul() {
        return this.http.get<number>(`${environment.apiUrl}/ecopontos/numberazul`);
    } 
    getNumberEcopontosAmarelo() {
        return this.http.get<number>(`${environment.apiUrl}/ecopontos/numberamarelo`);
    } 
    getNumberEcopontosVermelho() {
        return this.http.get<number>(`${environment.apiUrl}/ecopontos/numbervermelho`);
    } 
   

    getById(id: number) {
        return this.http.get<Ecoponto>(`${environment.apiUrl}/ecopontos/${id}`);
    }

    create(ecoponto: Ecoponto) {
        return this.http.post(`${environment.apiUrl}/ecopontos/create`, ecoponto);
    }

    update(ecoponto: Ecoponto) {
        console.log(ecoponto.codigoPostal);
        console.log(ecoponto.concelho);
        console.log(ecoponto.distrito);
        console.log(ecoponto.estado);
        console.log(ecoponto.freguesia);
        console.log(ecoponto.id);
        console.log(ecoponto.rua);
        console.log(ecoponto.sensor);
        console.log(ecoponto.tipo);

        return this.http.put(`${environment.apiUrl}/ecopontos/${ecoponto.id}`, ecoponto);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/ecopontos/${id}`);
    }
}