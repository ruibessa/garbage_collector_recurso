import { environment } from '../../environments/environment.prod';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Camiao, User } from '../_models';

@Injectable({ providedIn: 'root' })
export class CamiaoService {
    
    constructor(private http: HttpClient) { }

    getAll(id: number) {
        return this.http.get<Camiao[]>(`${environment.apiUrl}/camioes/list/${id}`);
    }

    getById(id: number) {
        return this.http.get<Camiao>(`${environment.apiUrl}/camioes/${id}`);
    }

    create(camiao: Camiao, userId: string) {
        camiao.empresa = userId;
        return this.http.post(`${environment.apiUrl}/camioes/register`, camiao);
    }

    getNumeroTotalCamioes() {
        return this.http.get<number>(`${environment.apiUrl}/camioes/totalCamioes`);
    } 
  

    update(camiao: Camiao, userId: string) {
        console.log(camiao.id);
        console.log(camiao.matricula);
        console.log(camiao.carga);
        console.log(camiao.distrito);
        console.log(camiao.concelho);
        console.log(camiao.empresa);
        console.log(userId);


        return this.http.put(`${environment.apiUrl}/camioes/${camiao.id}`, camiao);
    }

    delete(id: number) {
        return this.http.delete(`${environment.apiUrl}/camioes/${id}`);
    }
}