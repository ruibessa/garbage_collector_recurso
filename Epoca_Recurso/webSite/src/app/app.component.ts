﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AuthenticationService } from './_services';
import { User, Role } from './_models';
import * as Highcharts from 'highcharts';
import './_content/app.less';

@Component({ selector: 'app', templateUrl: 'app.component.html', styleUrls: ['app.component.css'] })
export class AppComponent {
    currentUser: User;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
        
        this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    }

    navbarOpen = false;

    toggleNavbar() {
        this.navbarOpen = !this.navbarOpen;
    }

    get isAdmin() {
        return this.currentUser && this.currentUser.role === Role.Admin;
    }

    get isGestor_Chefe() {
        return this.currentUser && this.currentUser.role === Role.Gestor_Chefe;
    }

    get isGestor() {
        return this.currentUser && this.currentUser.role === Role.Gestor;
    }

    updateUserNavbar() {
        this.router.navigate(['/updateUser']);
    }

    logout() {
        this.authenticationService.logout();
        this.router.navigate(['/login']);
    }
}